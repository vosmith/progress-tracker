!# /usr/bin/ruby

old_path = Dir.pwd
path = old_path
needle = ""
replacement = ""

while ARGV.length > 0
	arg = ARGV.shift
	if arg == "-p" || arg =="--path"
		path = ARGV.shift
	else
		if needle == ""
			needle = arg
		elsif replacement == ""
			replacement = arg
		else
			puts "\n\n\t You may only provide one (1) string to search for and one (1) string to replace it with\n\n"
			exit
		end	
	end 
end

puts "\n\nsearching for files matching #{needle} in #{path} and replacing with #{replacement}\n"

unless path.nil?
	files = Dir.entries(path)
	# puts files
else
	puts "\n\n\tThe path cannot be null!!\n\n"
	exit
end

Dir.chdir path

files.each do |f|
	if File.fnmatch(needle, f)
		#puts "\t#{f}"
		File.rename(f, f.gsub(needle, replacement))
	end
end

Dir.chdir old_path
