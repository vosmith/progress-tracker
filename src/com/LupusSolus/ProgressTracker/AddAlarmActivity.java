package com.LupusSolus.ProgressTracker;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.LupusSolus.ProgressTracker.dialogs.ConfirmCancelDialog;
import com.LupusSolus.ProgressTracker.receivers.AlarmReceiver;

import java.text.DateFormat;
import java.util.Calendar;

public class AddAlarmActivity extends FragmentActivity implements View.OnClickListener, TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {
    protected Calendar c;
    protected int year = 0;
    protected int month = 0;
    protected int day = 0;
    protected int hour = 0;
    protected int minute = 0;

    private static final String tag = "AddAlarmActivity";

    protected String trackName = "";
    protected int trackId = -1;

    protected static Button btnSetDay;
    protected static Button btnSetTime;
    protected static Spinner spnRepeat;

    protected static FragmentManager fm;

    public static CancelDialogOnClickListener cancelDialogListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        trackName = getIntent().getStringExtra("TRACK_NAME");
        trackId = getIntent().getIntExtra("TRACK_ID", -1);

        if (trackName != null) {
            setTitle(trackName + " Alarm");
            setContentView(R.layout.addalarm);

        }




		/*if(trackName == null){
            Log.d(tag, "No track name was passed in the intent");
			finish();
		}*/

        btnSetDay = (Button) findViewById(R.id.btn_pickDay);
        btnSetTime = (Button) findViewById(R.id.btn_pickTime);
        spnRepeat = (Spinner) findViewById(R.id.spn_repeat);

        ((TextView) findViewById(R.id.lbl_day)).setTypeface(MyApplication.font);
        ((TextView) findViewById(R.id.lbl_time)).setTypeface(MyApplication.font);
        ((TextView) findViewById(R.id.lbl_repeat)).setTypeface(MyApplication.font);

        btnSetDay.setOnClickListener(this);
        btnSetTime.setOnClickListener(this);

        Button saveAlarm = (Button) findViewById(R.id.btn_saveAlarm);
        Button cancelAlarm = (Button) findViewById(R.id.btn_cancelAlarm);


        cancelDialogListener = new CancelDialogOnClickListener();


        fm = getSupportFragmentManager();

        if (saveAlarm != null) {
            saveAlarm.setOnClickListener(this);
        }
        if (cancelAlarm != null) {
            cancelAlarm.setOnClickListener(this);
        }

        c = Calendar.getInstance();

        if (savedInstanceState != null) {
            year = savedInstanceState.getInt("YEAR");
            month = savedInstanceState.getInt("MONTH");
            day = savedInstanceState.getInt("DAY");
            hour = savedInstanceState.getInt("HOUR");
            minute = savedInstanceState.getInt("MINUTE");

            c.set(year, month, day, hour, minute);

            spnRepeat.setSelection(((ArrayAdapter) spnRepeat.getAdapter()).getPosition(savedInstanceState.get("REPEAT")));
        }

        if (Build.VERSION.SDK_INT > 11) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }


        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        if (trackName != null) {
            SharedPreferences pref = getSharedPreferences(trackName, MODE_PRIVATE);

            long time = pref.getLong("START", 0);
            String repeat = pref.getString("REPEAT", "None");
            if (year == 0) {
                if (time != 0) {
                    c.setTimeInMillis(time);
                }
                spnRepeat.setSelection(((ArrayAdapter) spnRepeat.getAdapter()).getPosition(repeat));
            }
        }

        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        btnSetDay.setText(DateFormat.getDateInstance(DateFormat.SHORT).format(c.getTime()));
        btnSetTime.setText(DateFormat.getTimeInstance(DateFormat.SHORT).format(c.getTime()));


        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.addalarmmenu, menu);
        return true;
    }


    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_pickDay:
                new DatePickerDialog(this, this, year, month, day).show();

//				DateTimeDialog dtd = new DateTimeDialog(year, month, day, hour, minute);
//				dtd.show(getSupportFragmentManager(), "dialog");
                break;
            case R.id.btn_pickTime:
                new TimePickerDialog(this, this, hour, minute, false).show();
                break;
            case R.id.btn_saveAlarm:
                saveAlarm();
                break;
            case R.id.btn_cancelAlarm:
                new ConfirmCancelDialog().show(fm, "confirm_cancel");
                break;
            default:
                break;
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        this.year = year;
        this.month = monthOfYear;
        this.day = dayOfMonth;

        c.set(this.year, this.month, this.day, this.hour, this.minute);

        btnSetDay.setText(DateFormat.getDateInstance(DateFormat.SHORT).format(c.getTime()));
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        this.hour = hourOfDay;
        this.minute = minute;

        c.set(this.year, this.month, this.day, this.hour, this.minute);

        btnSetTime.setText(DateFormat.getTimeInstance(DateFormat.SHORT).format(c.getTime()));
    }

	/*public void onClick(DialogInterface dialog, int which){
        switch(which){
			case DialogInterface.BUTTON_POSITIVE:
				c.set(DateTimeDialog.year, DateTimeDialog.month, DateTimeDialog.day, DateTimeDialog.hour, DateTimeDialog.minute);

                btnSetDate.setText(DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(c.getTime()));

				break;
			case DialogInterface.BUTTON_NEGATIVE:
				break;
			default:
				break;
		}
	}*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.menu_done:
                saveAlarm();
                break;
            case R.id.menu_discard:
                new ConfirmCancelDialog().show(fm, "confirm_cancel");
                break;
        }

        return true;
    }

    protected void saveAlarm() {
//        year = DateTimeDialog.year;
//        month = DateTimeDialog.month;
//        day = DateTimeDialog.day;
//        hour = DateTimeDialog.hour;
//        minute = DateTimeDialog.minute;

//        c.set(year, month, day, hour, minute);
        if (trackName != null) {
            SharedPreferences pref = getSharedPreferences(trackName, MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();

            editor.putLong("START", c.getTime().getTime());
            editor.putString("REPEAT", (String) spnRepeat.getSelectedItem());
            editor.putLong("LAST", 0);
            editor.commit();

            addAlarm();

            finish();
        }
    }

    private void discardAlarm() {
        if (trackName != null) {
            SharedPreferences pref2 = getSharedPreferences(trackName, MODE_PRIVATE);
            SharedPreferences.Editor editor2 = pref2.edit();

            editor2.clear();
            editor2.commit();

            cancelAlarm();

            finish();
        }
    }

    private void addAlarm() {
        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(this, AlarmReceiver.class);
        i.putExtra("TRACK_NAME", trackName);
        i.putExtra("TRACK_ID", trackId);

        PendingIntent alarm = PendingIntent.getBroadcast(this, getIntent().getIntExtra("TRACK_ID", -1), i, PendingIntent.FLAG_CANCEL_CURRENT);
        //Calendar c = Calendar.getInstance();
        //c.set(year, month, day, hour, minute);

        //Determine repeat frequency
        long repeatFreq = -1;
        switch (spnRepeat.getSelectedItemPosition()) {
            case 1:
                //1 hour
                repeatFreq = AlarmManager.INTERVAL_HOUR;
                break;
            case 2:
                //1 day
                repeatFreq = AlarmManager.INTERVAL_DAY;
                break;
            case 3:
                //1 week
                repeatFreq = AlarmManager.INTERVAL_DAY * 7;
                break;
            case 4:
                //1 month
                repeatFreq = AlarmManager.INTERVAL_DAY * 30;
                break;
            default:
                break;
        }

        Log.d(tag, "Setting alarm repeating for " + String.valueOf(c.getTime().getTime()) + " repeating every " + String.valueOf(repeatFreq));

        if (repeatFreq > 0) {
            am.setRepeating(AlarmManager.RTC_WAKEUP, c.getTime().getTime(), repeatFreq, alarm);
        } else {
            am.set(AlarmManager.RTC_WAKEUP, c.getTime().getTime(), alarm);
        }

        Toast.makeText(this, "Alarm set", Toast.LENGTH_LONG).show();
    }

    private void cancelAlarm() {
        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        Intent i = new Intent(this, AlarmReceiver.class);
        i.putExtra("TRACK_NAME", trackName);
        i.putExtra("TRACK_ID", getIntent().getIntExtra("TRACK_ID", -1));

        PendingIntent alarm = PendingIntent.getActivity(this, getIntent().getIntExtra("TRACK_ID", -1), i, PendingIntent.FLAG_CANCEL_CURRENT);

        am.cancel(alarm);

        Toast.makeText(this, "Alarm cleared", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("YEAR", year);
        outState.putInt("MONTH", month);
        outState.putInt("DAY", day);
        outState.putInt("HOUR", hour);
        outState.putInt("MINUTE", minute);
        outState.putString("REPEAT", (String) spnRepeat.getSelectedItem());

        super.onSaveInstanceState(outState);
    }

    private class CancelDialogOnClickListener implements Dialog.OnClickListener {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    Log.d(tag, "YOU SAID CANCEL ALARM!!");
                    try {
                        discardAlarm();
                        break;
                    } catch (Exception e) {
                        Log.e(tag, "Error deleting track", e);
                    }
                case DialogInterface.BUTTON_NEGATIVE:
                default:

                    break;
            }
        }
    }
}
