package com.LupusSolus.ProgressTracker.utils.image;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.util.Log;

public class BitmapLoaderManager {

    private static final String tag = "BitmapLoader";

    public enum Animator {NONE, FLY_IN_LEFT_SIDE, FLY_IN_RIGHT_SIDE, FLY_IN_BOTTOM, FLY_IN_TOP}

    ;

    /*private static int getSampleSize(int viewW, int viewH, int bitW, int bitH){
        Log.i(tag, String.format("determining sample size for %dx%d bitmap to %dx%d view", bitW, bitH, viewW, viewH));
        int hRatio = 1;
        int wRatio = 1;

        try{
            hRatio = (bitH / viewH) + 1;
        }catch(ArithmeticException e){}

        try{
            wRatio = (bitW / viewW) + 1;
        }catch(ArithmeticException e){}

        return hRatio > wRatio ? hRatio : wRatio;
    }*/

    public static Bitmap sizeAndOrient(String filename, int targetHeight, int targetWidth) {
        ExifInterface exif;
        int orientation = 1;

        try {
            exif = new ExifInterface(filename);
            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
        } catch (Exception ex) {
            Log.e(tag, "io error", ex);
        }

        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filename, opts);
        int h = opts.outHeight;
        int w = opts.outWidth;

        int sample = calculateSize(h, w, targetHeight, targetWidth);

        opts.inJustDecodeBounds = false;
        opts.inSampleSize = sample;

        //Decode Bitmap
        Bitmap b = BitmapFactory.decodeFile(filename, opts);

        Bitmap rotated;
        Matrix matrix;
        //Determine rotation
        switch (orientation) {
            case 6:
                matrix = new Matrix();
                matrix.postRotate(90);
                rotated = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
                break;
            case 3:
                matrix = new Matrix();
                matrix.postRotate(180);
                rotated = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
                break;
            case 8:
                matrix = new Matrix();
                matrix.postRotate(270);
                rotated = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
                break;
            default:
                rotated = b;
        }

        return rotated;
    }

    public static int calculateSize(int bitH, int bitW, int viewH, int viewW) {
        int sampleSize = 1;
        Log.d(tag, String.format("Bitmap size %dx%d, ImageView size %dx%d", bitW, bitH, viewW, viewH));
        if (bitH > viewH || bitW > viewW) {
            //Image is larger than the view

            int hScale = 1;
            int wScale = 1;
            try {
                hScale = (bitH / viewH) + 1;  //Integer division is kinda ugly... I add one to take care of any fractions
                Log.v(tag, String.format("bitmap : view height ratio = %d", hScale));
            } catch (ArithmeticException ex) {
            }

            try {
                wScale = (bitW / viewW) + 1;
                Log.v(tag, String.format("bitmap : view width ratio = %d", wScale));
            } catch (ArithmeticException ex) {
            }

            if (hScale > wScale) {
                Log.d(tag, String.format("Using %d as a sample size", hScale));
                sampleSize = hScale;
            } else {
                Log.d(tag, String.format("Using %d as a sample size", wScale));
                sampleSize = wScale;
            }
        }
        return sampleSize;
    }
}