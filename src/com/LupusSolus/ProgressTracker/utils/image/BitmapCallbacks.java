package com.LupusSolus.ProgressTracker.utils.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

/**
 * Created with IntelliJ IDEA.
 * User: vincent
 * Date: 1/28/13
 * Time: 7:46 AM
 * To change this template use File | Settings | File Templates.
 */
public class BitmapCallbacks implements LoaderManager.LoaderCallbacks<Bitmap> {
    private Context mCtx;
    private View mView;
    private String mTag;
    private static final String tag = "BitmapCallback";

    public BitmapCallbacks(Context ctx) {
        mCtx = ctx;
    }

    public BitmapCallbacks(Context ctx, View v) {
        mCtx = ctx;
        mView = v;
    }

    @Override
    public Loader onCreateLoader(int i, Bundle bundle) {

        Loader l = null;

        String filepath = bundle.getString("FILE");
        mTag = bundle.getString("TAG");
        BitmapLoaderManager.Animator a = (BitmapLoaderManager.Animator) bundle.getSerializable("ANIM");
        boolean cache = bundle.getBoolean("CACHE");

        l = new BitmapCacherTask(mCtx, filepath, 136, 136);
        Log.v(tag, "onCreateLoader with tag " + mTag);
        return l;
    }

    @Override
    public void onLoadFinished(Loader<Bitmap> loader, Bitmap b) {

        Log.d(tag, "onLoadFinished for view with tag " + ((BitmapCacherTask) loader).getTag());
        if (b == null) {
            Log.d(tag, "\tNo bitmap returned");
        } else {
            View target = mView.findViewWithTag(((BitmapCacherTask) loader).getTag());
            if (target != null) {
                TranslateAnimation ta = new TranslateAnimation(Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.ABSOLUTE, 0, Animation.ABSOLUTE, 0);
                ta.setDuration(200);
                target.setAnimation(ta);
                ((ImageView) target).setImageBitmap(b);
            }
            Log.d(tag, String.format("\tBitmap is %d bytes", b.getByteCount()));
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {
        Log.d(tag, "onLoaderReset");
    }
}
