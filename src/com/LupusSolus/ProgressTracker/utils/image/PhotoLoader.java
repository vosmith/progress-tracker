package com.LupusSolus.ProgressTracker.utils.image;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: vincent
 * Date: 1/22/13
 * Time: 7:17 AM
 * To change this template use File | Settings | File Templates.
 */
public class PhotoLoader extends AsyncTask<Boolean, Bitmap, Void> {
    private List<Map<String, Object>> views;
    private Context ctx;
    private ImageView target;
    private static final String pl_tag = "PhotoLoader";
    private Boolean animate = false;

    public PhotoLoader(List<Map<String, Object>> views, Context context) {
        this.views = views;
        this.ctx = context;
    }

    @Override
    protected Void doInBackground(Boolean... arg0) {
        Log.d(pl_tag, "Beginning loading process");

        if (arg0[0] == true) {
            animate = true;
        }
        for (Map<String, Object> view : views) {
            Log.d(pl_tag, "gathering information for view " + view.get("ID") + " and file " + view.get("FILE"));
            if (view.get("VIEW") != null) {
                target = (ImageView) view.get("VIEW");
            } else {
                target = (ImageView) ((Activity) ctx).findViewById(Integer.parseInt((String) (view.get("ID"))));
            }

            Log.d(pl_tag, "waiting for metrics.....");
            while (target.getHeight() == 0 && target.getWidth() == 0) {
                //do nothing
            }

            Log.d(pl_tag, "Resizing and orienting complete");

            publishProgress(sizeAndOrient((String) view.get("FILE"), target.getHeight(), target.getWidth()));
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Bitmap... bmp) {
        if (animate) {
            TranslateAnimation ta = new TranslateAnimation(Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.ABSOLUTE, 0, Animation.ABSOLUTE, 0);
            ta.setDuration(200);
            target.setAnimation(ta);
        }
        target.setImageBitmap(bmp[0]);
        target.requestLayout();
        super.onProgressUpdate(bmp);
    }

    public static int calculateSize(int bitH, int bitW, int viewH, int viewW) {
        int sampleSize = 1;
        Log.d("Photoloader", String.format("Bitmap size %dx%d, ImageView size %dx%d", bitW, bitH, viewW, viewH));
        if (bitH > viewH || bitW > viewW) {
            //Image is larger than the view

            int hScale = 1;
            int wScale = 1;
            try {
                hScale = (bitH / viewH) + 1;  //Integer division is kinda ugly... I add one to take care of any fractions
                Log.v("Photoloader", String.format("bitmap : view height ratio = %d", hScale));
            } catch (ArithmeticException ex) {
            }

            try {
                wScale = (bitW / viewW) + 1;
                Log.v("Photoloader", String.format("bitmap : view width ratio = %d", wScale));
            } catch (ArithmeticException ex) {
            }

            if (hScale > wScale) {
                Log.v("Photoloader", String.format("Using %d as a sample size", hScale));
                sampleSize = hScale;
            } else {
                Log.v("Photoloader", String.format("Using %d as a sample size", wScale));
                sampleSize = wScale;
            }
        }
        return sampleSize;
    }

    public static Bitmap sizeAndOrient(String filename, int targetHeight, int targetWidth) {
        ExifInterface exif;
        int orientation = 1;

        try {
            exif = new ExifInterface(filename);
            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
        } catch (Exception ex) {
            Log.e("PhotoLoader", "io error", ex);
        }

        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filename, opts);
        int h = opts.outHeight;
        int w = opts.outWidth;

        int sample = calculateSize(h, w, targetHeight, targetWidth);

        opts.inJustDecodeBounds = false;
        opts.inSampleSize = sample;

        //Decode Bitmap
        Bitmap b = BitmapFactory.decodeFile(filename, opts);

        Bitmap rotated;
        Matrix matrix;
        //Determine rotation
        switch (orientation) {
            case 6:
                matrix = new Matrix();
                matrix.postRotate(90);
                rotated = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
                break;
            case 3:
                matrix = new Matrix();
                matrix.postRotate(180);
                rotated = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
                break;
            case 8:
                matrix = new Matrix();
                matrix.postRotate(270);
                rotated = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
                break;
            default:
                rotated = b;
        }
        b = null;
        return rotated;
    }

    public static Bitmap sizeAndOrient(String filename, int targetHeight, int targetWidth, int minSampleSize) {
        ExifInterface exif;
        int orientation = 1;

        try {
            exif = new ExifInterface(filename);
            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
        } catch (Exception ex) {
            Log.e("PhotoLoader", "io error", ex);
        }

        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filename, opts);
        int h = opts.outHeight;
        int w = opts.outWidth;

        int sample = calculateSize(h, w, targetHeight, targetWidth);

        opts.inJustDecodeBounds = false;
        opts.inSampleSize = sample > minSampleSize ? sample : minSampleSize;
        Log.d("PhotoLoader", "sample size is: " + String.valueOf(opts.inSampleSize));
        //Decode Bitmap
        Bitmap b = BitmapFactory.decodeFile(filename, opts);

        Bitmap rotated;
        Matrix matrix;
        //Determine rotation
        switch (orientation) {
            case 6:
                matrix = new Matrix();
                matrix.postRotate(90);
                rotated = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
                break;
            case 3:
                matrix = new Matrix();
                matrix.postRotate(180);
                rotated = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
                break;
            case 8:
                matrix = new Matrix();
                matrix.postRotate(270);
                rotated = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
                break;
            default:
                rotated = b;
        }
        b = null;
        return rotated;
    }
}
