package com.LupusSolus.ProgressTracker.utils.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.LupusSolus.ProgressTracker.R;
import com.LupusSolus.ProgressTracker.utils.MyHelper;

/**
 * Created with IntelliJ IDEA.
 * User: vincent
 * Date: 1/28/13
 * Time: 7:48 AM
 * To change this template use File | Settings | File Templates.
 */
public class BitmapLoaderTask extends AsyncTaskLoader<Bitmap> {
    private Context mCtx;
    private String mUri;
    private View mRoot;
    private ImageView mView;
    private String mTag;
    private BitmapLoaderManager.Animator mAnim = null;
    private static final String tag = "BitmapLoader";
    private boolean mCache = false;

    public BitmapLoaderTask(Context ctx, String filePath, String viewTag, BitmapLoaderManager.Animator anim, boolean cache) {
        super(ctx);
        mCtx = ctx;
        mUri = filePath;
        mAnim = anim;
        mTag = viewTag;
        mCache = cache;
        Log.v(tag, "BitmapLoader Created\n\tUrl: " + mUri + "\n\tView Tag: " + mTag);
    }

    public BitmapLoaderTask(Context ctx, String filePath, String viewTag, boolean cache) {
        super(ctx);
        mCtx = ctx;
        mUri = filePath;
        mAnim = BitmapLoaderManager.Animator.NONE;
        mTag = viewTag;
        mCache = cache;
        Log.v(tag, "BitmapLoader Created\n\tUrl: " + mUri + "\n\tView Tag: " + mTag);
    }

    public BitmapLoaderTask(Context ctx, View v, String filePath, String viewTag, boolean cache) {
        super(ctx);
        mCtx = ctx;
        mRoot = v;
        mUri = filePath;
        mAnim = BitmapLoaderManager.Animator.NONE;
        mTag = viewTag;
        mCache = cache;
        Log.v(tag, "BitmapLoader Created\n\tUrl: " + mUri + "\n\tView Tag: " + mTag);
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public Bitmap loadInBackground() {
        Log.d(tag, "starting to load image....");

        if (mRoot != null) {
            Log.d(tag, "mRoot not null: " + mRoot.toString());
            mView = (ImageView) mRoot.findViewWithTag(mTag);
            if (mView != null) {
                Log.d(tag, "found mView with tag: " + mTag);
            } else {
                Log.d(tag, "mView is null");
                return null;
            }
        } else {
            LinearLayout l = (LinearLayout) ((FragmentActivity) mCtx).findViewById(R.id.main_list);
            Log.d(tag, "got linear layout: " + l.toString());

            //while(mView == null){
            mView = (ImageView) l.findViewWithTag(mTag);

            if (mView == null) {
                return null;
            }
            //}
        }
        //View ll =  l.getChildAt(0);
        //Log.d(tag, "got imageView : " + mView.toString());
        //AdapterView av =(AdapterView) ll.getChildAt(0);
        //Log.d(tag, "got adapter view");

        //AdapterView av =(AdapterView) ((LinearLayout) ((FragmentActivity) mCtx).findViewById(R.id.main_list)).getChildAt(0);
        //if(av == null){
        //    Log.e(tag, "Could not find the main list view");
        //}else{
        //    Log.d(tag, "Adapter view is not null");
        //}

        //mView = (ImageView) av.findViewWithTag(mTag);

//        if(mView == null){
//            Log.e(tag, "Could not find the view with tag " + mTag);
//            return null;
//        }

        Log.v(tag, "waiting for metrics");
        while (mView.getHeight() == 0 && mView.getWidth() == 0) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                Log.e(tag, "error sleeping thread", e);
            }
        }

        Log.v(tag, "Creating bitmap");

        return BitmapLoaderManager.sizeAndOrient(mUri, mView.getHeight(), mView.getWidth());
        //return null;
    }

    @Override
    public void deliverResult(Bitmap data) {
        if (data != null) {
            TranslateAnimation ta;
            switch (mAnim) {
                case FLY_IN_LEFT_SIDE:
                    ta = new TranslateAnimation(Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.ABSOLUTE, 0, Animation.ABSOLUTE, 0);
                    ta.setDuration(200);
                    mView.setAnimation(ta);
                    break;
                case FLY_IN_RIGHT_SIDE:
                    ta = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.ABSOLUTE, 0, Animation.ABSOLUTE, 0);
                    ta.setDuration(200);
                    mView.setAnimation(ta);
                    break;
                case FLY_IN_TOP:
                    ta = new TranslateAnimation(Animation.ABSOLUTE, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0);
                    ta.setDuration(200);
                    mView.setAnimation(ta);
                    break;
                case FLY_IN_BOTTOM:
                    ta = new TranslateAnimation(Animation.ABSOLUTE, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0);
                    ta.setDuration(200);
                    mView.setAnimation(ta);
                    break;

            }
            Log.d(tag, "setting bitmap");
            mView.setImageBitmap(data);
            if (mCache) {
                Log.d(tag, "\t\tCACHE-ING");
                MyHelper.bitmapCache.put(mUri.split("/")[mUri.split("/").length - 2], data);
            }
        }
        super.deliverResult(data);
    }
}
