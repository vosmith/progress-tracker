package com.LupusSolus.ProgressTracker.utils.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.LupusSolus.ProgressTracker.utils.MyHelper;

/**
 * Created with IntelliJ IDEA.
 * User: vincent
 * Date: 1/28/13
 * Time: 7:48 AM
 * To change this template use File | Settings | File Templates.
 */
public class BitmapCacherTask extends AsyncTaskLoader<Bitmap> {
    private Context mCtx;
    private String mUri;
    private int mHeight;
    private int mWidth;

    private static final String tag = "BitmapLoader";

    public BitmapCacherTask(Context ctx, String filePath, int height, int width) {
        super(ctx);
        mCtx = ctx;
        mUri = filePath;
        mHeight = height;
        mWidth = width;

        Log.v(tag, "BitmapCacher Created\n\tUrl: " + mUri);
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
        super.onStartLoading();
    }

    @Override
    public Bitmap loadInBackground() {
        Log.d(tag, "starting to load image....");

        return BitmapLoaderManager.sizeAndOrient(mUri, mHeight, mWidth);
        //return null;
    }

    @Override
    public void deliverResult(Bitmap data) {
        if (data != null) {

            Log.d(tag, "setting bitmap");

            Log.d(tag, "\t\tCACHE-ING");
            MyHelper.bitmapCache.put(mUri.split("/")[mUri.split("/").length - 2], data);
        }
        super.deliverResult(data);
    }

    public String getTag() {
        return mUri.split("/")[mUri.split("/").length - 2];
    }
}
