package com.LupusSolus.ProgressTracker.utils.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

public class BitmapLoader extends AsyncTaskLoader<Bitmap> {
    private Context mCtx;
    private String mUri;
    private ImageView mView;
    private static final String tag = "BitmapLoader";
    private Animator mAnim = null;

    public enum Animator {NONE, FLY_IN_LEFT_SIDE, FLY_IN_RIGHT_SIDE, FLY_IN_BOTTOM, FLY_IN_TOP}

    ;

    public BitmapLoader(Context ctx, String filePath, ImageView view, Animator anim) {
        super(ctx);
        mCtx = ctx;
        mUri = filePath;
        mAnim = anim;
        mView = view;
        Log.v(tag, "BitmapLoader Created\n\tUrl: " + mUri + "\n\tView ID: " + String.valueOf(mView.getId()));
    }

    public BitmapLoader(Context ctx, String filePath, ImageView view) {
        super(ctx);
        mCtx = ctx;
        mUri = filePath;
        mAnim = Animator.NONE;
        mView = view;
        Log.v(tag, "BitmapLoader Created\n\tUrl: " + mUri + "\n\tView ID: " + String.valueOf(mView.getId()));
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    protected void onReset() {
        //forceLoad();
    }

    @Override
    public Bitmap loadInBackground() {
        Log.d(tag, "starting to load image....");
        //View v = ((Activity) mCtx).findViewById(mViewId);

        Log.v(tag, "waiting for metrics");
        while (mView.getHeight() == 0 && mView.getWidth() == 0) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                Log.e(tag, "error sleeping thread", e);
            }
        }
        /*while(v.getWidth() == 0){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                Log.e(tag, "error sleeping thread", e);
            }
        }*/

        //BitmapFactory.Options opts = new BitmapFactory.Options();
        //opts.inJustDecodeBounds = true;

        //Log.v(tag, "Getting bitmap dimensions for " + mUri);
        //BitmapFactory.decodeFile(mUri, opts);

        //int sampleSize = getSampleSize(mView.getWidth(), mView.getHeight(), opts.outWidth, opts.outHeight);

        //Log.d(tag, String.format("using sample size %d", sampleSize));
        //opts.inJustDecodeBounds = false;
        //opts.inSampleSize = sampleSize;

        Log.d(tag, "Creating bitmap");
        //Bitmap b = BitmapFactory.decodeFile(mUri, opts);

        return sizeAndOrient(mUri, mView.getHeight(), mView.getWidth());
    }

    @Override
    public void deliverResult(Bitmap data) {
//        View v = ((Activity) mCtx).findViewById(mViewId);
        TranslateAnimation ta;
        switch (mAnim) {
            case FLY_IN_LEFT_SIDE:
                ta = new TranslateAnimation(Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.ABSOLUTE, 0, Animation.ABSOLUTE, 0);
                ta.setDuration(200);
                mView.setAnimation(ta);
                break;
            case FLY_IN_RIGHT_SIDE:
                ta = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.ABSOLUTE, 0, Animation.ABSOLUTE, 0);
                ta.setDuration(200);
                mView.setAnimation(ta);
                break;
            case FLY_IN_TOP:
                ta = new TranslateAnimation(Animation.ABSOLUTE, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0);
                ta.setDuration(200);
                mView.setAnimation(ta);
                break;
            case FLY_IN_BOTTOM:
                ta = new TranslateAnimation(Animation.ABSOLUTE, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0);
                ta.setDuration(200);
                mView.setAnimation(ta);
                break;

        }

        mView.setImageBitmap(data);
        Log.d(tag, mUri);

        //MyHelper.bitmapCache.put(mUri.split("/")[mUri.split("/").length - 2], data);
        super.deliverResult(data);
    }

    private static int getSampleSize(int viewW, int viewH, int bitW, int bitH) {
        Log.i(tag, String.format("determining sample size for %dx%d bitmap to %dx%d view", bitW, bitH, viewW, viewH));
        int hRatio = 1;
        int wRatio = 1;

        try {
            hRatio = (bitH / viewH) + 1;
        } catch (ArithmeticException e) {
        }

        try {
            wRatio = (bitW / viewW) + 1;
        } catch (ArithmeticException e) {
        }

        return hRatio > wRatio ? hRatio : wRatio;
    }

    public static Bitmap sizeAndOrient(String filename, int targetHeight, int targetWidth) {
        ExifInterface exif;
        int orientation = 1;

        try {
            exif = new ExifInterface(filename);
            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
        } catch (Exception ex) {
            Log.e(tag, "io error", ex);
        }

        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filename, opts);
        int h = opts.outHeight;
        int w = opts.outWidth;

        int sample = calculateSize(h, w, targetHeight, targetWidth);

        opts.inJustDecodeBounds = false;
        opts.inSampleSize = sample;

        //Decode Bitmap
        Bitmap b = BitmapFactory.decodeFile(filename, opts);

        Bitmap rotated;
        Matrix matrix;
        //Determine rotation
        switch (orientation) {
            case 6:
                matrix = new Matrix();
                matrix.postRotate(90);
                rotated = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
                break;
            case 3:
                matrix = new Matrix();
                matrix.postRotate(180);
                rotated = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
                break;
            case 8:
                matrix = new Matrix();
                matrix.postRotate(270);
                rotated = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
                break;
            default:
                rotated = b;
        }
        b = null;
        return rotated;
    }

    public static int calculateSize(int bitH, int bitW, int viewH, int viewW) {
        int sampleSize = 1;
        Log.d(tag, String.format("Bitmap size %dx%d, ImageView size %dx%d", bitW, bitH, viewW, viewH));
        if (bitH > viewH || bitW > viewW) {
            //Image is larger than the view

            int hScale = 1;
            int wScale = 1;
            try {
                hScale = (bitH / viewH) + 1;  //Integer division is kinda ugly... I add one to take care of any fractions
                Log.v(tag, String.format("bitmap : view height ratio = %d", hScale));
            } catch (ArithmeticException ex) {
            }

            try {
                wScale = (bitW / viewW) + 1;
                Log.v(tag, String.format("bitmap : view width ratio = %d", wScale));
            } catch (ArithmeticException ex) {
            }

            if (hScale > wScale) {
                Log.v(tag, String.format("Using %d as a sample size", hScale));
                sampleSize = hScale;
            } else {
                Log.v(tag, String.format("Using %d as a sample size", wScale));
                sampleSize = wScale;
            }
        }
        return sampleSize;
    }
}