package com.LupusSolus.ProgressTracker.utils;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.LupusSolus.ProgressTracker.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: vincent
 * Date: 1/30/13
 * Time: 9:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class TypeAdapter extends BaseAdapter {
    private List<Map<Integer, String>> types;
    private Context mCtx;
    private List<String> allowedTypes;
    private static final String tag = "SpinnerAdapter";

    public TypeAdapter(Context ctx) {
        Log.d(tag, "Creating adapter");
        try {
            types = TrackDB.getAllTypes();
        } catch (Exception e) {
            types = new ArrayList<Map<Integer, String>>();
        }
        allowedTypes = new ArrayList<String>();
        allowedTypes.add("general");

        mCtx = ctx;

    }

    @Override
    public int getCount() {
        return types.size();
    }

    @Override
    public Object getItem(int position) {
        return types.get(position);
    }

    @Override
    public long getItemId(int position) {
        return (Integer) types.get(position).keySet().toArray()[0];
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return allowedTypes.contains(types.get(position).values().toArray()[0]);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d(tag, "getView called");
        if (convertView == null) {
            Log.d(tag, "\tconvertView was null");
            LayoutInflater li = (LayoutInflater) mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.category_spinneritem, null);

            if (isEnabled(position)) {
                ((TextView) convertView).setText(((String) types.get(position).values().toArray()[0]));
            } else {
                ((TextView) convertView).setText(types.get(position).values().toArray()[0] + "...Pro only");
                convertView.setEnabled(false);
            }
        }

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        Log.d(tag, "getDropDownView called");
        if (convertView == null) {
            Log.d(tag, "\tconvertView was null");
            LayoutInflater li = (LayoutInflater) mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
//            convertView = li.inflate(R.layout.category_spinneritem, null);

            if (isEnabled(position)) {
                ((TextView) convertView).setText(((String) types.get(position).values().toArray()[0]));
//            ((TextView)((LinearLayout) convertView).getChildAt(0)).setText(((String) types.get(position).values().toArray()[0]));
            } else {
                ((TextView) convertView).setText(((String) types.get(position).values().toArray()[0]) + "...Pro only");
//            ((TextView)((LinearLayout) convertView).getChildAt(0)).setText(types.get(position).values().toArray()[0] + "...Pro only" );
                convertView.setEnabled(false);
            }
        }

        return convertView;
    }

}
