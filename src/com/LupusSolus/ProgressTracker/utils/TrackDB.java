package com.LupusSolus.ProgressTracker.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.LupusSolus.ProgressTracker.datatypes.GeneralData;
import com.LupusSolus.ProgressTracker.datatypes.StepData;
import com.LupusSolus.ProgressTracker.datatypes.WeightLossData;
import com.LupusSolus.ProgressTracker.models.Step;
import com.LupusSolus.ProgressTracker.models.Track;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TrackDB {
    private static MyHelper h;
    private static SQLiteDatabase db;

    private static Integer trackIdIndex;
    private static Integer trackNameIndex;
    private static Integer trackDescrIndex;
    private static Integer trackCreatedIndex;
    private static Integer trackModifiedIndex;
    private static Integer trackTypeIndex;

    private static Integer stepIdIndex;
    private static Integer stepTypeIndex;
    private static Integer stepNotesIndex;
    private static Integer stepCreatedIndex;
    private static Integer stepTrackIdIndex;

    private static Integer typeIdIndex;
    private static Integer typeNameIndex;
    private static Integer typeFormatIndex;

    private static final String tag = "TrackDB";

    public static void open(Context ctx) throws SQLException {
        if (db == null) {
            if (h == null) {
                h = new MyHelper(ctx);
            }
            db = h.getWritableDatabase();
        } else {
            if (!db.isOpen()) {
                db = h.getWritableDatabase();
            }
        }
    }

    private static void dbCheck() throws IOException {
        if (db == null) {
            if (h == null) {
                throw new IOException("Database has not been initialized");
            }
            db = h.getWritableDatabase();
        } else {
            if (!db.isOpen()) {
                db = h.getWritableDatabase();
            }
        }
    }

    public static List<Track> getAllTracks() throws IOException {
        dbCheck();
        List<Track> retList = new ArrayList<Track>();

        Cursor res = db.query(MyHelper.TRACK_TABLE, null, null, null, null, null, "created ASC");
        res.moveToFirst();

        checkIndeces("track", res);

        while (!res.isAfterLast()) {
            Log.v(tag, String.format("Track ID: %d, Track name: %s", res.getInt(trackIdIndex), res.getString(trackNameIndex)));
            Track t = new Track(res.getInt(trackIdIndex), res.getString(trackNameIndex), res.getString(trackDescrIndex), res.getInt(trackTypeIndex), new Date(res.getLong(trackCreatedIndex)),
                    new Date(res.getLong(trackModifiedIndex)));
            retList.add(t);
            res.moveToNext();
        }
        res.close();
        return retList;
    }

    public static List<Map<String, String>> getSimpleAdapterTracks() throws IOException {
        dbCheck();
        List<Map<String, String>> retList = new ArrayList<Map<String, String>>();

        Cursor res = db.query(MyHelper.TRACK_TABLE, null, null, null, null, null, "created ASC");
        res.moveToFirst();

        checkIndeces("track", res);

        while (!res.isAfterLast()) {
            Log.v(tag, String.format("Track ID: %d, Track name: %s", res.getInt(trackIdIndex), res.getString(trackNameIndex)));
            //Track t = new Track(res.getInt(trackIdIndex), res.getString(trackNameIndex), res.getString(trackDescrIndex), res.getInt(trackTypeIndex), new Date(res.getLong(trackCreatedIndex)),
            //        new Date(res.getLong(trackModifiedIndex)));
            Map<String, String> map = new HashMap<String, String>();

            map.put("NAME", res.getString(trackNameIndex));
            map.put("DATE", "Last Updated: " + new Date(res.getLong(trackModifiedIndex)).toLocaleString());


            //retList.add(t);
            retList.add(map);
            res.moveToNext();
        }
        res.close();
        return retList;
    }

    public static int createTrack(Track newTrack) throws SQLiteConstraintException, IOException {
        dbCheck();
        Cursor res = db.query(MyHelper.TRACK_TABLE, null, MyHelper.TRACK_NAME_COL + "=?", new String[]{newTrack.getName()}, null, null, null);

        if (res.getCount() == 0) {
            ContentValues CV = new ContentValues();
            CV.put(MyHelper.TRACK_NAME_COL, newTrack.getName());
            CV.put(MyHelper.TRACK_DESCR_COL, newTrack.getDescription());
            CV.put(MyHelper.TRACK_TYPE_COL, newTrack.getType());
            CV.put(MyHelper.TRACK_CREATE_COL, newTrack.getCreated().getTime());
            return (int) db.insert(MyHelper.TRACK_TABLE, null, CV);
        } else {
            return -1;
        }
    }

    public static void saveTrack(Track t) throws SQLException, IOException {
        dbCheck();
        ContentValues CV = new ContentValues();
        CV.put(MyHelper.TRACK_NAME_COL, t.getName());
        CV.put(MyHelper.TRACK_DESCR_COL, t.getDescription());
        CV.put(MyHelper.TRACK_MOD_COL, t.getModified().getTime());

        db.update(MyHelper.TRACK_TABLE, CV, "id=?", new String[]{String.valueOf(t.getId())});
    }

    public static Track findTrack(int trackId) throws IOException {
        dbCheck();
        Cursor res = db.query(MyHelper.TRACK_TABLE, null, MyHelper.TRACK_ID_COL + "=?", new String[]{String.valueOf(trackId)}, null, null, "created ASC");
        res.moveToFirst();

        checkIndeces("track", res);

        if (res.getCount() > 0) {
            Track t = new Track(res.getInt(trackIdIndex), res.getString(trackNameIndex), res.getString(trackDescrIndex), res.getInt(trackTypeIndex), new Date(res.getLong(trackCreatedIndex)),
                    new Date(res.getLong(trackModifiedIndex)));
            res.close();
            return t;
        } else {
            return null;
        }

    }

    public static void deleteTrack(Track t) throws IOException {
        dbCheck();
        db.delete(MyHelper.STEP_TABLE, MyHelper.STEP_TRACK_COL + "=?", new String[]{String.valueOf(t.getId())});
        db.delete(MyHelper.TRACK_TABLE, "id=?", new String[]{String.valueOf(t.getId())});
    }

    public static int createStep(Step s) throws IOException {
        dbCheck();
        if (s.getNotes().isVerified()) {
            ContentValues CV = new ContentValues();
            CV.put(MyHelper.STEP_TRACK_COL, s.getTrack().getId());
            CV.put(MyHelper.STEP_TYPE_COL, s.getType());
            CV.put(MyHelper.STEP_NOTES_COL, s.getNotes().toString());
            CV.put(MyHelper.STEP_CREATE_COL, s.getCreated().getTime());

            Log.d(tag, "inserting step into db");
            Log.d(tag, "TrackID" + String.valueOf(s.getTrack().getId()));
            Log.d(tag, "Type" + s.getType());
            Log.d(tag, "Notes" + s.getNotes().toString());
            Log.d(tag, "Time" + String.valueOf(s.getCreated().getTime()));

            return (int) db.insert(MyHelper.STEP_TABLE, null, CV);
        } else {
            Log.d(tag, "notes are not verified");
            return -1;
        }
    }

    public static boolean editStep(Step s) throws IOException {
        dbCheck();

        ContentValues CV = new ContentValues();
        CV.put(MyHelper.STEP_NOTES_COL, s.getNotes().toString());

        if (db.update(MyHelper.STEP_TABLE, CV, "id=?", new String[]{String.valueOf(s.getID())}) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static List<Step> findSteps(int trackId) throws JSONException, IOException {
        dbCheck();
        List<Step> retList = new ArrayList<Step>();
        Log.d(tag, "getting steps for Track Id: " + String.valueOf(trackId));
        Cursor res = db.query(MyHelper.STEP_TABLE, null, MyHelper.STEP_TRACK_COL + "=?", new String[]{String.valueOf(trackId)}, null, null, "created DESC");
        if (res.getCount() > 0) {
            res.moveToFirst();

            checkIndeces("step", res);

            StepData data = null;
            while (!res.isAfterLast()) {
                switch (res.getInt(stepTypeIndex)) {
                    case StepData.TYPE_GENERAL:
                        data = new GeneralData(new JSONObject(res.getString(stepNotesIndex)));
                        break;
                    case StepData.TYPE_WEIGHT_LOSS:
                        data = new WeightLossData(new JSONObject(res.getString(stepNotesIndex)));
                }


                Step s = new Step(res.getInt(stepIdIndex), res.getInt(stepTrackIdIndex), res.getInt(stepTypeIndex), data, new Date(res.getLong(stepCreatedIndex)));
                retList.add(s);
                res.moveToNext();
            }
        }
        res.close();
        return retList;
    }

    public static List<Map<Integer, String>> getAllTypes() throws IOException {
        dbCheck();
        List<Map<Integer, String>> retList = new ArrayList<Map<Integer, String>>();

        Cursor res = db.query(MyHelper.TYPE_TABLE, null, null, null, null, null, null);
        res.moveToFirst();

        checkIndeces("type", res);

        while (!res.isAfterLast()) {
            Map<Integer, String> m = new HashMap<Integer, String>();
            m.put(res.getInt(typeIdIndex), res.getString(typeNameIndex));
            retList.add(m);
            res.moveToNext();
        }

        res.close();
        return retList;
    }

    public static void close() throws Exception, SQLException {
        if (db == null) {
            throw new Exception("A connection to the database was never made");
        } else {
            if (db.isOpen()) {
                Log.i("TrackDB", "closed successfully");
                db.close();
            } else {
                Log.i("TrackDB", "database was already closed");
            }
        }
    }

    private static void checkIndeces(String table, Cursor c) {
        if (table.toLowerCase().equals("track")) {
            if (trackIdIndex == null) {
                trackIdIndex = c.getColumnIndex(MyHelper.TRACK_ID_COL);
                trackNameIndex = c.getColumnIndex(MyHelper.TRACK_NAME_COL);
                trackDescrIndex = c.getColumnIndex(MyHelper.TRACK_DESCR_COL);
                trackTypeIndex = c.getColumnIndex(MyHelper.TRACK_TYPE_COL);
                trackCreatedIndex = c.getColumnIndex(MyHelper.TRACK_CREATE_COL);
                trackModifiedIndex = c.getColumnIndex(MyHelper.TRACK_MOD_COL);
            }
        } else if (table.toLowerCase().equals("step")) {
            if (stepIdIndex == null) {
                stepIdIndex = c.getColumnIndex(MyHelper.STEP_ID_COL);
                stepNotesIndex = c.getColumnIndex(MyHelper.STEP_NOTES_COL);
                stepTypeIndex = c.getColumnIndex(MyHelper.STEP_TYPE_COL);
                stepCreatedIndex = c.getColumnIndex(MyHelper.STEP_CREATE_COL);
                stepTrackIdIndex = c.getColumnIndex(MyHelper.STEP_TRACK_COL);

            }
        } else if (table.toLowerCase().equals("type")) {
            if (typeIdIndex == null) {
                typeIdIndex = c.getColumnIndex(MyHelper.TYPE_ID_COL);
                typeNameIndex = c.getColumnIndex(MyHelper.TYPE_NAME_COL);
                typeFormatIndex = c.getColumnIndex(MyHelper.TYPE_FORMAT_COL);

            }
        }
    }

}
