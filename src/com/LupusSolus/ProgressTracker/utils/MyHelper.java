package com.LupusSolus.ProgressTracker.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.os.Environment;
import android.support.v4.util.LruCache;
import android.util.Log;

import com.LupusSolus.ProgressTracker.datatypes.StepData;

public class MyHelper extends SQLiteOpenHelper {
    private static final String CREATE_TRACKS = "CREATE TABLE IF NOT EXISTS Tracks(id integer primary key autoincrement, name string unique not null, description string, type integer not null, created integer not null, modified integer);";
    private static final String CREATE_STEPS = "CREATE TABLE IF NOT EXISTS Steps(id integer primary key autoincrement, track_id integer not null, type integer not null, created integer not null, notes string);";
    private static final String CREATE_TYPES = "CREATE TABLE IF NOT EXISTS Types(id integer primary key autoincrement, name string not null, format string not null);";

    public static final String TRACK_TABLE = "Tracks";
    public static final String STEP_TABLE = "Steps";
    public static final String TYPE_TABLE = "Types";

    public static final String TRACK_ID_COL = "id";
    public static final String TRACK_NAME_COL = "name";
    public static final String TRACK_DESCR_COL = "description";
    public static final String TRACK_TYPE_COL = "type";
    public static final String TRACK_CREATE_COL = "created";
    public static final String TRACK_MOD_COL = "modified";

    public static final String STEP_ID_COL = "id";
    public static final String STEP_TRACK_COL = "track_id";
    public static final String STEP_NOTES_COL = "notes";
    public static final String STEP_TYPE_COL = "type";
    public static final String STEP_CREATE_COL = "created";

    public static final String TYPE_ID_COL = "id";
    public static final String TYPE_NAME_COL = "name";
    public static final String TYPE_FORMAT_COL = "format";

    public static final String TEMP_ROOT = Environment.getExternalStorageDirectory() + "/ProgressTracker/temp/";
    public static final String TRACKS_ROOT = Environment.getExternalStorageDirectory() + "/ProgressTracker/Tracks/";
    public static LruCache<String, Bitmap> bitmapCache = new LruCache<String, Bitmap>(4 * 1024 * 1024) {
        @Override
        protected int sizeOf(String key, Bitmap value) {
            return value.getByteCount();
        }
    };

    public MyHelper(Context context) {
        super(context, "TrackDB", null, 2);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i("TrackDB", "creating database for first time use");
        db.execSQL(CREATE_TRACKS);
        db.execSQL(CREATE_STEPS);
        db.execSQL(CREATE_TYPES);
        seedDb(db);
        Log.i("TrackDB", "Database successfully created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i("TrackDB", "upgrading from version" + String.valueOf(oldVersion) + " to " + String.valueOf(newVersion));

        switch (oldVersion) {
            case 1:
                db.execSQL(CREATE_TYPES);

                seedDb(db);

                db.execSQL("ALTER TABLE " + TRACK_TABLE + " ADD COLUMN " + TRACK_TYPE_COL + " integer;");
                db.execSQL("ALTER TABLE " + STEP_TABLE + " ADD COLUMN " + STEP_TYPE_COL + " integer;");

                ContentValues CV = new ContentValues();
                CV.put(TRACK_TYPE_COL, 1);

                db.update(TRACK_TABLE, CV, null, null);

                CV = new ContentValues();
                CV.put(STEP_TYPE_COL, 1);
                db.update(STEP_TABLE, CV, null, null);

                Cursor c = db.query(STEP_TABLE, new String[]{STEP_ID_COL, STEP_NOTES_COL}, null, null, null, null, null);
                c.moveToFirst();
                while (!c.isAfterLast()) {
                    String data = c.getString(c.getColumnIndex(STEP_NOTES_COL));
                    CV = new ContentValues();
                    CV.put(STEP_NOTES_COL, "{notes:\"" + data + "\"}");
//				int index = c.getColumnIndex(STEP_ID_COL);
//				int id = c.getInt(index);
//				String test = String.valueOf(id);
                    db.update(STEP_TABLE, CV, STEP_ID_COL + "=?", new String[]{String.valueOf(c.getInt(c.getColumnIndex(STEP_ID_COL)))});
                    c.moveToNext();
                }

            default:
                break;
        }

        Log.i("TrackDB", "DB upgrade complete");
    }

    private void seedDb(SQLiteDatabase db) {
        ContentValues CV = new ContentValues();
        CV.put(TYPE_ID_COL, StepData.TYPE_GENERAL);
        CV.put(TYPE_NAME_COL, "general");
        CV.put(TYPE_FORMAT_COL, "{notes:\"string\"}");
        db.insert(TYPE_TABLE, null, CV);

        CV = new ContentValues();
        CV.put(TYPE_ID_COL, StepData.TYPE_WEIGHT_LOSS);
        CV.put(TYPE_NAME_COL, "weight loss");
        CV.put(TYPE_FORMAT_COL, "{weight:{value:\"float\",unit:\"string\"},waist:{value:\"float\",unit:\"string\"}, bmi:\"integer\", notes:\"string\"}");
        db.insert(TYPE_TABLE, null, CV);

        CV = new ContentValues();
        CV.put(TYPE_ID_COL, StepData.TYPE_PLANT_GROWTH);
        CV.put(TYPE_NAME_COL, "plant growth");
        CV.put(TYPE_FORMAT_COL, "{height:{value:\"float\",unit:\"string\"}, branches:\"integer\", leaves:\"integer\", flowers:\"integer\", notes:\"string\"}");
        db.insert(TYPE_TABLE, null, CV);

        CV = new ContentValues();
        CV.put(TYPE_ID_COL, StepData.TYPE_MOVEMENT);
        CV.put(TYPE_NAME_COL, "movement");
        CV.put(TYPE_FORMAT_COL, "{distance:{value:\"float\",unit:\"string\"},notes:\"string\"}");
        db.insert(TYPE_TABLE, null, CV);
    }

}
