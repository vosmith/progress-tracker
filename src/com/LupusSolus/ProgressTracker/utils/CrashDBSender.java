package com.LupusSolus.ProgressTracker.utils;

import android.util.Log;

import org.acra.ReportField;
import org.acra.collector.CrashReportData;
import org.acra.sender.ReportSender;
import org.acra.sender.ReportSenderException;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: vincent
 * Date: 3/18/13
 * Time: 9:38 AM
 * To change this template use File | Settings | File Templates.
 */
public class CrashDBSender implements ReportSender {
    private static final String URL = "http://crash-db.herokuapp.com/crashes/new";

    @Override
    public void send(CrashReportData crashReportData) throws ReportSenderException {
        HttpClient client = new DefaultHttpClient();

        HttpPost req = new HttpPost(URL);
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("stacktrace", crashReportData.get(ReportField.STACK_TRACE)));
        params.add(new BasicNameValuePair("project", "PicTr"));

        try {
            req.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
            client.execute(req);

        } catch (IOException e) {
            Log.e("crasher", "Can you do anything right??", e);
        }
    }
}
