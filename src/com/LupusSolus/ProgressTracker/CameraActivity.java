package com.LupusSolus.ProgressTracker;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.os.Bundle;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageButton;
import android.widget.SeekBar;

import com.LupusSolus.ProgressTracker.utils.MyHelper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CameraActivity extends Activity implements Callback,
        OnClickListener, Camera.ShutterCallback, Camera.PictureCallback, SeekBar.OnSeekBarChangeListener {
    private SurfaceView sv;
    private SurfaceHolder holder;
    private OrientationEventListener oel;
    private static Camera mCamera;
    private static Parameters mCamParams;
    private int flashState = 0;

    private static enum Zooms {
        ZOOM,
        SMOOTH_ZOOM,
        NONE
    }

    ;

    //private static List<String> flashModes;
    //private static List<String> focusModes;
    //private static List<Integer> zoomRatios;
    private static Zooms zoomType;
    private static Integer currentZoom = 0;
    private static Integer maxZoom = 0;
    //private static Integer minZoom = 0;

    private static int rotation = 0;
    private static int lastRotation = 0;

    private static ImageButton btnCancel;
    private static ImageButton btnTakePic;
    private static ImageButton btnFlash;
    private static ImageButton btnSwitch;

    private static SeekBar seekZoom;

    private static int currentCamera = 0;
    /* private static Handler h = new Handler() {
         @Override
         public void handleMessage(Message msg) {
             Log.d(tag, "Message received");
             switch (msg.what) {
                 case 1:
                     Log.d(tag, "\tANIMATE!!!");
                     btnCancel.animate();
                     btnTakePic.animate();
                     break;
             }
             super.handleMessage(msg);
         }
     };*/
    //private int frames;
    //private long startTime = 0;
    private static final String tag = "CameraActivity";
    private static final String settings_tag = "CameraSettings";
//	private Preview mPreview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.cameraview);
        sv = (SurfaceView) findViewById(R.id.surface);
        sv.setOnClickListener(this);

        btnCancel = ((ImageButton) findViewById(R.id.btn_cancel));
        btnCancel.setOnClickListener(this);

        btnTakePic = ((ImageButton) findViewById(R.id.btn_takePic));
        btnTakePic.setOnClickListener(this);

        btnFlash = (ImageButton) findViewById(R.id.btn_flash);
        btnFlash.setImageDrawable(getResources().getDrawable(R.drawable.flash_state));
        btnFlash.setOnClickListener(this);

        seekZoom = (SeekBar) findViewById(R.id.seek_zoom);
        seekZoom.setOnSeekBarChangeListener(this);

        btnSwitch = (ImageButton) findViewById(R.id.btn_switch);
        btnSwitch.setOnClickListener(this);

        oel = new OrientationEventListener(this) {
            public void onOrientationChanged(int orientation) {
                try {
                    //Get Camera rotation info
                    CameraInfo camInfo = new CameraInfo();
                    Camera.getCameraInfo(currentCamera, camInfo);

                    orientation = (orientation + 45) / 90 * 90;
                    rotation = 0;

                    if (camInfo.facing == CameraInfo.CAMERA_FACING_FRONT) {
                        rotation = (camInfo.orientation - orientation + 360) % 360;
                    } else {  // back-facing camera
                        rotation = (camInfo.orientation + orientation) % 360;
                    }

                    Log.d(tag, "Orientation is: " + String.valueOf(orientation));
                    Log.d(tag, "Rotation is: " + String.valueOf(rotation));

                    if (rotation != lastRotation && rotation != 270) {
                        Log.d(tag, String.format("\tshould rotate by %f", ((float) rotation) - ((float) lastRotation)));

                        RotateAnimation ra = new RotateAnimation((-1) * lastRotation, (-1) * rotation, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                        RotateAnimation ra1 = new RotateAnimation((-1) * lastRotation, (-1) * rotation, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                        RotateAnimation ra2 = new RotateAnimation((-1) * lastRotation, (-1) * rotation, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                        RotateAnimation ra3 = new RotateAnimation((-1) * lastRotation, (-1) * rotation, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

                        ra.setDuration(500);
                        ra.setRepeatCount(0);
                        ra.setFillAfter(true);

                        ra1.setDuration(500);
                        ra1.setRepeatCount(0);
                        ra1.setFillAfter(true);

                        ra2.setDuration(500);
                        ra2.setRepeatCount(0);
                        ra2.setFillAfter(true);

                        ra3.setDuration(500);
                        ra3.setRepeatCount(0);
                        ra3.setFillAfter(true);

                        btnCancel.startAnimation(ra);
                        btnTakePic.startAnimation(ra1);
                        btnFlash.startAnimation(ra2);
                        btnSwitch.startAnimation(ra3);
                    }

                    lastRotation = rotation;
                    mCamParams.setRotation(rotation);
                    mCamera.setParameters(mCamParams);

                } catch (Exception e) {
                    Log.e(tag, "Error on orientation change", e);
                }
            }
        };

        if(Camera.getNumberOfCameras() < 2){
            btnSwitch.setVisibility(View.GONE);
        }

        super.onCreate(savedInstanceState);
    }


    @Override
    protected void onResume() {
        try {
            Log.i(tag, "Opening camera");
            mCamera = Camera.open(currentCamera);
            mCamParams = mCamera.getParameters();

            mCamParams.setFocusMode(Parameters.FOCUS_MODE_AUTO);
            mCamera.setParameters(mCamParams);

            SharedPreferences prefs = getSharedPreferences("camera_prefs", MODE_PRIVATE);
            flashState = prefs.getInt("FLASH", 0);

            setFlashState();

            oel.enable();

            sv = (SurfaceView) findViewById(R.id.surface);
            Log.i(tag, "Getting holder");
            holder = sv.getHolder();

            Log.i(tag, "adding callback");
            holder.addCallback(this);
            holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

            /* Learn about the camera features */

            if (mCamParams.isSmoothZoomSupported()) {
                zoomType = Zooms.SMOOTH_ZOOM;
            } else if (mCamParams.isZoomSupported()) {
                zoomType = Zooms.ZOOM;
            } else {
                zoomType = Zooms.NONE;
            }

            //zoomRatios = mCamParams.getZoomRatios();

            currentZoom = mCamParams.getZoom();
            maxZoom = mCamParams.getMaxZoom();

            /*Log.d(settings_tag, "Zoom Information {");
            Log.d(settings_tag, "\tZoom Type : " + zoomType);
            Log.d(settings_tag, "\tZoom Ratios : [");
            for (Integer val : zoomRatios) {
                Log.d(settings_tag, "\t\t" + val.toString());
            }*/
            Log.d(settings_tag, "\t]");
            Log.d(settings_tag, "Current Zoom : " + currentZoom);
            Log.d(settings_tag, "Maximum Zoom : " + maxZoom);
            Log.d(settings_tag, "}");


            seekZoom.setMax(mCamParams.getMaxZoom());
        } catch (Exception e) {
            Log.e(getString(R.string.app_name), "failed to open Camera");
            e.printStackTrace();
        }
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_takePic:
                mCamera.takePicture(this, new Camera.PictureCallback() {
                    @Override
                    public void onPictureTaken(byte[] data, Camera camera) {
                        //Do Nothing this is for the rawData callback, the activity will handle the jpeg callback
                    }
                }, this);
/**/
                break;
            case R.id.btn_cancel:
                setResult(RESULT_CANCELED);
                finish();
                break;

            case R.id.btn_flash:
                if (flashState < 2) {
                    flashState++;
                } else {
                    flashState = 0;
                }

                setFlashState();

                break;
            case R.id.surface:
                mCamera.autoFocus(new Camera.AutoFocusCallback() {
                    @Override
                    public void onAutoFocus(boolean success, Camera camera) {
                        //Do Nothing
                    }
                });
                break;
            case R.id.btn_switch:
                mCamera.stopPreview();
                mCamera.release();

                currentCamera = currentCamera == 0 ? 1 : 0;

                mCamera = Camera.open(currentCamera);
                try {
                    mCamera.setPreviewDisplay(holder);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mCamera.startPreview();
        }

    }

    private void setFlashState() {
        switch (flashState) {
            case 0:
                mCamParams.setFlashMode(Parameters.FLASH_MODE_AUTO);
                break;
            case 1:
                mCamParams.setFlashMode(Parameters.FLASH_MODE_OFF);
                break;
            case 2:
                mCamParams.setFlashMode(Parameters.FLASH_MODE_ON);
                break;
        }

        Log.d(settings_tag, "setting flash state to " + String.valueOf(flashState));
        btnFlash.setImageLevel(flashState);

        mCamera.setParameters(mCamParams);
    }

    @Override
    protected void onPause() {
        oel.disable();

        mCamera.stopPreview();
        mCamera.setPreviewCallback(null);
        mCamera.release();
        mCamera = null;

        SharedPreferences prefs = getSharedPreferences("camera_prefs", MODE_PRIVATE);
        prefs.edit().putInt("FLASH", flashState).commit();

        super.onDestroy();
    }

    /*
    Surface Handler implementations
     */

    @Override
    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
        //Do Nothing
        try {
            mCamera.setPreviewDisplay(holder);
            holder.setSizeFromLayout();
            Log.i(tag, "starting preview");
            mCamera.startPreview();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder arg0) {
        Log.i(tag, "setting holder for preview");
        try {
            mCamera.setPreviewDisplay(holder);
            holder.setSizeFromLayout();
            Log.i(tag, "starting preview");
            mCamera.startPreview();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder arg0) {
        //Do Nothing
    }

    /*
    Picture and Shutter Callback implementations
     */

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        File f = new File(MyHelper.TEMP_ROOT + "image.jpg");
        File tmpDir = new File(MyHelper.TEMP_ROOT);
        if (!tmpDir.exists()) {
            tmpDir.mkdirs();
        }
        try {

            Matrix m = new Matrix();
            CameraInfo info = new CameraInfo();
            mCamera.getCameraInfo(currentCamera, info);

            if(info.facing == CameraInfo.CAMERA_FACING_FRONT){
                m.preScale(-1, 1);
            }

            Bitmap b = BitmapFactory.decodeByteArray(data, 0, data.length);
            b = Bitmap.createBitmap(b,0,0, b.getWidth(), b.getHeight(), m, true);
            FileOutputStream fos = new FileOutputStream(f);

            b.compress(Bitmap.CompressFormat.JPEG, 90, fos);

            fos.flush();
            fos.close();
            setResult(RESULT_OK);
        } catch (FileNotFoundException e) {
            Log.d(tag, "File not found " + f.getAbsolutePath());
            e.printStackTrace();
            setResult(400);
        } catch (IOException e) {
            Log.d(tag, "IO Error " + f.getAbsolutePath());
            e.printStackTrace();
            setResult(400);
        }
        finish();
    }

    @Override
    public void onShutter() {
        //Do Nothing
    }

    /*
    SeekBar Change implementations
     */
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        Log.v(settings_tag, "progress changed");
        //if(fromUser){
        Log.v(settings_tag, "\tfrom user, Level is: " + String.valueOf(progress));
        mCamParams.setZoom(progress);
        mCamera.setParameters(mCamParams);
        //}
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        Log.v(settings_tag, "zoom started");
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        Log.v(settings_tag, "zoom ended");
    }
}
