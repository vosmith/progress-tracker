package com.LupusSolus.ProgressTracker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ViewFlipper;

public class FirstTrackActivity extends Activity implements OnTouchListener, OnClickListener {
    //TODO: Handle Screen Rotation

    private GestureDetector gd;
    protected ViewFlipper vf;
    private Button skipBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.firsttracks);
        vf = (ViewFlipper) findViewById(R.id.flpr_firstTrack);
        skipBtn = (Button) findViewById(R.id.btn_firstTrackAction);

        gd = new GestureDetector(new SwipeListener());
        vf.setOnTouchListener(this);
        skipBtn.setOnClickListener(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onTouch(View arg0, MotionEvent arg1) {
        return gd.onTouchEvent(arg1);
    }

    private class SwipeListener extends SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if (e1.getX() > e2.getX() && Math.abs(velocityX) > 1200) {
                if (vf.getDisplayedChild() < vf.getChildCount() - 1) {
                    vf.showNext();
                    if (vf.getDisplayedChild() == vf.getChildCount() - 1) {
                        skipBtn.setText("Get Started!");
                    }
                }
                return false;
            } else if (e1.getX() < e2.getX() && Math.abs(velocityX) > 1200) {
                if (vf.getDisplayedChild() > 0) {
                    vf.showPrevious();
                }
                return false;
            } else {
                return true;
            }
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_firstTrackAction:
                startActivity(new Intent(this, AddTrackActivity.class));
                finish();
                break;
        }
    }
}
