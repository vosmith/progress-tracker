package com.LupusSolus.ProgressTracker;

import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.LupusSolus.ProgressTracker.models.Track;
import com.LupusSolus.ProgressTracker.utils.MyHelper;
import com.LupusSolus.ProgressTracker.utils.TrackDB;
import com.LupusSolus.ProgressTracker.utils.TypeAdapter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddTrackActivity extends AddAlarmActivity implements OnClickListener, CompoundButton.OnCheckedChangeListener {
    private EditText txtName;
    //private EditText txtDescr;
    //private ImageButton btnPhoto;
    private CheckBox chkAlarm;
    private Spinner spnType;
    private ViewGroup alarmCont;

    private boolean killTmp = true;
    private boolean picTaken = false;

//    private Calendar c;
//    private int year = 0;
//    private int month = 0;
//    private int day = 0;
//    private int hour = 0;
//    private int minute = 0;

//    private static Button btnSetDay;
//    private static Button btnSetTime;
//    private static Spinner spnRepeat;

    //public static CancelDialogOnClickListener cancelDialogListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.addtrack);

        txtName = (EditText) findViewById(R.id.txt_addTrackName);
        //txtDescr = (EditText) findViewById(R.id.txt_addTrackDescr);
        //btnPhoto = (ImageButton) findViewById(R.id.btn_firstStep);
        spnType = (Spinner) findViewById(R.id.spn_addTrackType);
        chkAlarm = (CheckBox) findViewById(R.id.chk_alarm);
        alarmCont = (ViewGroup) findViewById(R.id.alarm_container);

        final Button addBtn = (Button) findViewById(R.id.btn_addTrack);

        if (savedInstanceState != null) {
            picTaken = savedInstanceState.getBoolean("PIC_TAKEN");
            killTmp = savedInstanceState.getBoolean("KILL_TMP");
        }

//		if(picTaken){
//			loadImage();
//		}

        //btnPhoto.setOnClickListener(this);
        chkAlarm.setOnCheckedChangeListener(this);

        if (addBtn != null) {
            addBtn.setOnClickListener(this);
        }
        //TypeAdapter typeAdapter = new TypeAdapter(this);

        spnType.setAdapter(new TypeAdapter(this));
        try {
            List<Map<Integer, String>> typeList = TrackDB.getAllTypes();

            List<Map<String, String>> adapterList = new ArrayList<Map<String, String>>();
            for (int position = 0; position < typeList.size(); position++) {
                Map<String, String> stringMap = new HashMap<String, String>();
                stringMap.put("name", (String) typeList.get(position).values().toArray()[0]);
                adapterList.add(stringMap);
            }


//            spnType.setAdapter(new SimpleAdapter(this, adapterList, android.R.layout.simple_dropdown_item_1line, new String[]{"name"}, new int[]{android.R.id.text1}));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (killTmp) {
            File tmp = new File(MyHelper.TEMP_ROOT + "image.jpg");
            if (tmp.exists()) {
                tmp.delete();
            }
        } else {
            killTmp = true;
        }
        ActionBar ab = getActionBar();

        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }

        super.onCreate(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_pickDay:
                new DatePickerDialog(this, this, year, month, day).show();

//				DateTimeDialog dtd = new DateTimeDialog(year, month, day, hour, minute);
//				dtd.show(getSupportFragmentManager(), "dialog");
                break;
            case R.id.btn_pickTime:
                new TimePickerDialog(this, this, hour, minute, false).show();
                break;
            case R.id.btn_addTrack:
                addTrack();
                break;
            default:
                Toast.makeText(this, "Add this view to your OnClick method", Toast.LENGTH_LONG).show();
                break;
        }

		/*switch (arg0.getId()) {
        case R.id.btn_firstStep:
			File tmpDir = new File(MyHelper.TEMP_ROOT);
			File tracksDir = new File(MyHelper.TRACKS_ROOT);
			if (!tmpDir.exists()) {
				if (!tmpDir.mkdirs() || !tracksDir.mkdirs()) {
					Toast.makeText(this, "Error creating tmpDir",
							Toast.LENGTH_LONG);
				} else {
					Intent i = new Intent(this, CameraActivity.class);
					startActivityForResult(i, 1);
				}
			} else {
				Intent i = new Intent(this, CameraActivity.class);
				startActivityForResult(i, 1);
			}

			break;*/

			/*String name = txtName.getText().toString();
            String descr = txtDescr.getText().toString();
			int type = (int) spnType.getSelectedItemId();

			if(name.equals("") || type == 0){
				Toast.makeText(this,"You must have a name and a type",Toast.LENGTH_LONG).show();
				return;
			}

			try {
				Track t = new Track(name, descr, type);
				t.setId(TrackDB.createTrack(t));
				if (t.getId() != -1) {
					File newTrackDir = new File(MyHelper.TRACKS_ROOT + name);
					newTrackDir.mkdirs();

					Toast.makeText(this, "Track created!", Toast.LENGTH_SHORT).show();

					Intent i = new Intent(this, AddStepActivity.class);
					i.putExtra("TRACK_ID", t.getId());
					i.putExtra("TRACK_NAME", t.getName());
					i.putExtra("PIC_TAKEN", picTaken);
					startActivity(i);

					finish();
				} else {
					Toast.makeText(this, "A track named: " + name + " already exists.", Toast.LENGTH_LONG).show();
				}
			} catch (NullPointerException npe) {
				Toast.makeText(this, npe.getMessage(), Toast.LENGTH_SHORT).show();

			} catch (Exception ex) {
				Toast.makeText(this,"An error occurred while creating your track",Toast.LENGTH_LONG).show();
				Log.e("Add Track", ex.toString(), ex);
			}
			break;

		default:
			Toast.makeText(this, "Add this view to your OnClick method", Toast.LENGTH_LONG).show();
		}
       */
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.chk_alarm:
                if (isChecked) {
                    Animation a = AnimationUtils.loadAnimation(this, R.anim.fadein);
                    a.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            alarmCont.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {
                        }
                    });

                    alarmCont.startAnimation(a);
                } else {
                    Animation a = AnimationUtils.loadAnimation(this, R.anim.fadeout);
                    a.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            alarmCont.setVisibility(View.INVISIBLE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {
                        }
                    });

                    alarmCont.startAnimation(a);
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.addtrackmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_addtrack:
                addTrack();
                break;
            case R.id.menu_cancel:
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    /*private void loadImage(){
        List<Map<String, Object>> tempPhotoList = new ArrayList<Map<String, Object>>();
		Map<String, Object> tempPhotoMap = new HashMap<String, Object>();
		tempPhotoMap.put("FILE", MyHelper.TEMP_ROOT + "image.jpg");
		tempPhotoMap.put("ID", String.valueOf(R.id.btn_firstStep));
		tempPhotoList.add(tempPhotoMap);			
			
		PhotoLoader pl = new PhotoLoader(tempPhotoList, this);
			
		pl.execute(false);
	}*/

    private void addTrack() {
        String name = txtName.getText().toString();
        //String descr = txtDescr.getText().toString();
        int type = (int) spnType.getSelectedItemId();

        if (name.equals("") || type == 0) {
            Toast.makeText(this, "You must have a name and a type", Toast.LENGTH_LONG).show();
            return;
        }

        try {
            Track t = new Track(name, "", type);
            t.setId(TrackDB.createTrack(t));
            if (t.getId() != -1) {
                File newTrackDir = new File(MyHelper.TRACKS_ROOT + name);
                newTrackDir.mkdirs();

                Toast.makeText(this, "Track created!", Toast.LENGTH_SHORT).show();

                trackName = t.getName();
                trackId = t.getId();

                if (chkAlarm.isChecked()) {
                    trackName = name;
                    saveAlarm();
                }

                Intent i = new Intent(this, AddStepActivity.class);
                i.putExtra("TRACK_ID", t.getId());
                i.putExtra("TRACK_NAME", t.getName());
                i.putExtra("PIC_TAKEN", picTaken);
                startActivity(i);

                finish();
            } else {
                Toast.makeText(this, "A track named: " + name + " already exists.", Toast.LENGTH_LONG).show();
            }
        } catch (NullPointerException npe) {
            Toast.makeText(this, npe.getMessage(), Toast.LENGTH_SHORT).show();

        } catch (Exception ex) {
            Toast.makeText(this, "An error occurred while creating your track", Toast.LENGTH_LONG).show();
            Log.e("Add Track", ex.toString(), ex);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    //loadImage();
                    picTaken = true;
                } else if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(this, "Camera cancelled", Toast.LENGTH_LONG)
                            .show();
                } else {
                    Toast.makeText(this, "Camera error", Toast.LENGTH_LONG).show();
                }
                break;
            default:
                Toast.makeText(this, "request code not recognized",
                        Toast.LENGTH_LONG).show();
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("PIC_TAKEN", picTaken);
        outState.putBoolean("KILL_TEMP", false);

        super.onSaveInstanceState(outState);
    }


}
