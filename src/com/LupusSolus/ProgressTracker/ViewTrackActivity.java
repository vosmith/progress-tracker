package com.LupusSolus.ProgressTracker;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GestureDetectorCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextMenu;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.LupusSolus.ProgressTracker.models.Step;
import com.LupusSolus.ProgressTracker.models.Track;
import com.LupusSolus.ProgressTracker.utils.MyHelper;
import com.LupusSolus.ProgressTracker.utils.TrackDB;
import com.LupusSolus.ProgressTracker.utils.image.BitmapLoader;
import com.LupusSolus.ProgressTracker.utils.image.PhotoLoader;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ViewTrackActivity extends FragmentActivity implements OnClickListener, View.OnTouchListener {
    private static Track track;
    private static Step activeStep;
    private static int index;
    private static ImageView img;
    private LinearLayout stepsView;
    private static TextView stepInfo;

    private static GestureDetectorCompat gd;

    private final int NEW_STEP_REQ = 100;
    private final int EDIT_STEP_REQ = 101;

    public static final int NEW_STEP_SUCCESS = 200;
    public static final int NEW_STEP_CANCELLED = 300;
    public static final int NEW_STEP_FAILURE = 400;
    private boolean rebuild;

    private static final String tag = "ViewTrack";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.viewtrack);

        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        int id = getIntent().getIntExtra("TRACK_ID", -1);
        if (id < 0) {
            Toast.makeText(this, "Track could not be found", Toast.LENGTH_LONG)
                    .show();
        } else {
            try {
                track = TrackDB.findTrack(id);
            } catch (Exception e) {
                Log.e(tag, "Error", e);
                startActivity(new Intent(this, SplashActivity.class));
                finish();
            }
        }

        gd = new GestureDetectorCompat(this, new MyGestureListener(this));

        img = (ImageView) findViewById(R.id.img_mainStep);
        stepsView = (LinearLayout) findViewById(R.id.scroll_steps);

        stepInfo = (TextView) findViewById(R.id.txt_stepInfo);
        stepInfo.setTypeface(MyApplication.boldFont);

        img.setOnTouchListener(this);

        if (Build.VERSION.SDK_INT > 11) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }

        registerForContextMenu(img);

        setTitle(track.getName());
        setStep(this, 0);
        rebuild = true;
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        if (rebuild) {

            stepsView.removeAllViews();

            int idCount = 1;
            for (Step s : track.getAllSteps()) {
                ImageView img = new ImageView(this);

                LayoutParams lp;
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                    lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
                } else {
                    lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                }

                img.setTag("Thumbnail");
                img.setOnClickListener(this);
                img.setId(idCount);

                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);

                int margin = Math.round(metrics.density * 4);

                lp.setMargins(margin, margin, margin, margin);

                stepsView.addView(img, lp);

                idCount++;

                BitmapLoader.Animator a;
                int orientation = getResources().getConfiguration().orientation;

                if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                    a = BitmapLoader.Animator.FLY_IN_BOTTOM;
                } else if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    a = BitmapLoader.Animator.FLY_IN_RIGHT_SIDE;
                } else {
                    a = BitmapLoader.Animator.NONE;
                    Toast.makeText(this, "You need to work on getting your orientation", Toast.LENGTH_SHORT).show();
                }
                new BitmapLoader(this, MyHelper.TRACKS_ROOT + track.getName() + "/" + String.valueOf(s.getID()) + ".jpg", img, a).startLoading();
            }

            setStep(this, 0);
            rebuild = false;
        }
        super.onResume();
    }

    private static void setStep(Context mCtx, int index) {
        Log.v(tag, "Setting step with index " + String.valueOf(index));
        activeStep = track.getStep(index);
        if (activeStep != null && track != null) {
            List<Map<String, Object>> views = new ArrayList<Map<String, Object>>();

            Map<String, Object> m = new HashMap<String, Object>();
            m.put("ID", String.valueOf(R.id.img_mainStep));
            m.put("FILE", MyHelper.TRACKS_ROOT + track.getName() + "/" + String.valueOf(activeStep.getID()) + ".jpg");

            views.add(m);

            PhotoLoader mainLoader = new PhotoLoader(views, mCtx);

            mainLoader.execute(false);

            //img.setImageBitmap(PhotoLoader.sizeAndOrient(MyHelper.TRACKS_ROOT + track.getName() + "/" + activeStep.getID() + ".jpg", img.getHeight(),img.getWidth()));
            Log.d(tag, "Notes for the step: " + activeStep.getNotes().toString());
            stepInfo.setText(activeStep.getNotes().toListString());
        } else {
            stepInfo.setText("No steps taken!");
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.editstepmenu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit_step:
                Intent i = new Intent(this, AddStepActivity.class);
                i.putExtra("EDIT_MODE", true);
                i.putExtra("TRACK_ID", track.getId());
                i.putExtra("TRACK_NAME", track.getName());
                i.putExtra("STEP_ID", activeStep.getID());

                startActivityForResult(i, EDIT_STEP_REQ);
                break;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_newStep:
                Intent i = new Intent(this, AddStepActivity.class);
                i.putExtra("TRACK_ID", track.getId());
                i.putExtra("TRACK_NAME", track.getName());
                startActivityForResult(i, NEW_STEP_REQ);
                break;
            default:
                if (v.getTag().equals("Thumbnail")) {
                    index = ((LinearLayout) v.getParent()).indexOfChild(v);
                    setStep(this, index);
                }
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case NEW_STEP_REQ:
                switch (resultCode) {
                    case NEW_STEP_SUCCESS:
                        rebuild = true;

                        break;
                    case NEW_STEP_CANCELLED:
                        rebuild = false;
                        break;
                    case NEW_STEP_FAILURE:
                        rebuild = false;
                        break;
                }

                if (resultCode == NEW_STEP_FAILURE) {
                    Toast.makeText(this,
                            "An error occurred while saving your step",
                            Toast.LENGTH_LONG).show();
                } else if (resultCode == NEW_STEP_SUCCESS) {

                }
                break;
            case EDIT_STEP_REQ:
                switch (resultCode) {
                    case NEW_STEP_SUCCESS:
                        rebuild = true;

                        break;
                    case NEW_STEP_CANCELLED:
                        rebuild = false;
                        break;
                    case NEW_STEP_FAILURE:
                        rebuild = false;
                        break;
                }

                if (resultCode == NEW_STEP_FAILURE) {
                    Toast.makeText(this, "An error occurred while saving your step", Toast.LENGTH_LONG).show();
                }
                break;
            default:
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.viewtrackmenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.slideshow:
//			Intent slideshowIntent = new Intent(this, SlideshowActivity.class);
                Intent slideshowIntent = new Intent(this, SlideshowActivityCopy.class);
                slideshowIntent.putExtra("TRACK_ID", track.getId());
                slideshowIntent.putExtra("TRACK_NAME", track.getName());

                startActivity(slideshowIntent);
                break;
            case R.id.set_alarm:
                Intent addAlarmIntent = new Intent(this, AddAlarmActivity.class);
                addAlarmIntent.putExtra("TRACK_ID", track.getId());
                addAlarmIntent.putExtra("TRACK_NAME", track.getName());

                startActivity(addAlarmIntent);
                break;
            case R.id.new_step:
                Intent newStepIntent = new Intent(this, AddStepActivity.class);
                newStepIntent.putExtra("TRACK_ID", track.getId());
                newStepIntent.putExtra("TRACK_NAME", track.getName());
                startActivityForResult(newStepIntent, NEW_STEP_REQ);
                break;
            case R.id.menu_settings:
                startActivity(new Intent(this, AlarmSettingsActivity.class));
                break;
            case R.id.edit_step:
                Intent i = new Intent(this, AddStepActivity.class);
                i.putExtra("EDIT_MODE", true);
                i.putExtra("TRACK_ID", track.getId());
                i.putExtra("TRACK_NAME", track.getName());
                i.putExtra("STEP_ID", activeStep.getID());

                startActivity(i);
                break;
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        rebuild = true;
        super.onDestroy();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        gd.onTouchEvent(event);

        return false;
    }

    private static class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
        private static Context mCtx;

        public MyGestureListener(Context ctx) {
            super();
            mCtx = ctx;
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            if (activeStep != null) {
                Intent intent = new Intent(mCtx, ViewStepActivity.class);
                intent.putExtra("TRACK_ID", track.getId());
                intent.putExtra("STEP_ID", activeStep.getID());
                mCtx.startActivity(intent);
            }
            return super.onSingleTapUp(e);
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if ((e1.getX() > e2.getX()) && (Math.abs(e1.getX() - e2.getX()) > 100)) {
                //SWIPE TO LEFT
                if (index < track.getStepCount() - 1) {
                    setStep(mCtx, ++index);
                }
            } else if ((e1.getX() < e2.getX()) && (Math.abs(e1.getX() - e2.getX()) > 100)) {
                //SWIPE TO RIGHT
                if (index > 0) {
                    setStep(mCtx, --index);
                }
            }
            return super.onFling(e1, e2, velocityX, velocityY);
        }
    }
}
