package com.LupusSolus.ProgressTracker.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;

import com.LupusSolus.ProgressTracker.MainActivity;

/**
 * Created with IntelliJ IDEA.
 * User: vincent
 * Date: 1/18/13
 * Time: 7:49 AM
 * To change this template use File | Settings | File Templates.
 */
public class ConfimDeleteDialog extends DialogFragment {
    private static final String tag = "confirmDelete";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Log.d(tag, "Creating dialog");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage("All photos related to this track will be deleted")
                .setTitle("Are you sure?")
                .setPositiveButton("Delete", MainActivity.cancelDialogListener)
                .setNegativeButton("Cancel", MainActivity.cancelDialogListener)
                .setCancelable(true);

        return builder.create();
    }
}
