package com.LupusSolus.ProgressTracker.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;

import com.LupusSolus.ProgressTracker.AddAlarmActivity;

/**
 * Created with IntelliJ IDEA.
 * User: vincent
 * Date: 1/18/13
 * Time: 7:49 AM
 * To change this template use File | Settings | File Templates.
 */
public class ConfirmCancelDialog extends DialogFragment {
    private static final String tag = "confirmCancel";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Log.d(tag, "Creating dialog");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage("This alarm will be discarded.")
                .setTitle("Are you sure?")
                .setPositiveButton("Discard", AddAlarmActivity.cancelDialogListener)
                .setNegativeButton("Cancel", AddAlarmActivity.cancelDialogListener)
                .setCancelable(true);

        return builder.create();
    }
}
