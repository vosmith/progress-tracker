package com.LupusSolus.ProgressTracker.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.LupusSolus.ProgressTracker.R;

public class DateTimeDialog extends DialogFragment implements DatePicker.OnDateChangedListener, TimePicker.OnTimeChangedListener {
    public static int year;
    public static int month;
    public static int day;
    public static int hour;
    public static int minute;

    public DateTimeDialog() {

    }

    public DateTimeDialog(int y, int m, int d, int hr, int min) {
        year = y;
        month = m;
        day = d;
        hour = hr;
        minute = min;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater li = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //d.setOnDateChangedListener(getActivity());
        View dtpView = li.inflate(R.layout.datetimedialog, null);

        //Dialog d = builder.setTitle("Select Date and Time").setPositiveButton("Done", (AddAlarmActivity) getActivity()).setNegativeButton("Cancel", (AddAlarmActivity) getActivity()).setView(dtpView).create();

        DatePicker dp = (DatePicker) dtpView.findViewById(R.id.dp_setDate);
        dp.init(year, month, day, this);

        TimePicker tp = (TimePicker) dtpView.findViewById(R.id.tp_setTime);
        tp.setCurrentHour(hour);
        tp.setCurrentMinute(minute);

        tp.setOnTimeChangedListener(this);

        return null;

    }

    public void onDateChanged(DatePicker picker, int yr, int mo, int d) {
        year = yr;
        month = mo;
        day = d;
    }

    public void onTimeChanged(TimePicker picker, int hr, int min) {
        hour = hr;
        minute = min;
    }
}
