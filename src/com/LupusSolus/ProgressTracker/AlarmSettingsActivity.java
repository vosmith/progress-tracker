package com.LupusSolus.ProgressTracker;

import android.app.ActionBar;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.preference.RingtonePreference;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;

/**
 * Created with IntelliJ IDEA.
 * User: vincent
 * Date: 1/10/13
 * Time: 2:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class AlarmSettingsActivity extends PreferenceActivity {
    private PreferenceScreen screen;
    private RingtonePreference ringtonePref;
    private static final String tag = "settings";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.alarm_prefs);
        screen = getPreferenceScreen();

        ringtonePref = (RingtonePreference) screen.findPreference("alarm_tone");

        ringtonePref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                displayTitle(newValue.toString());

                return true;
            }
        });

        displayTitle(screen.getSharedPreferences().getString("alarm_tone", ""));

        ActionBar ab = getActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void displayTitle(String ringtoneUri) {
        if (ringtoneUri.equals(Settings.System.DEFAULT_ALARM_ALERT_URI.toString())) {
            ringtonePref.setSummary("Default");
        } else {
            Cursor c = getContentResolver().query(Uri.parse(ringtoneUri), new String[]{MediaStore.MediaColumns.TITLE}, null, null, null);

            if (c != null) {
                c.moveToFirst();
                Log.d(tag, String.format("Found %d result(s) matching your URI", c.getCount()));

                ringtonePref.setSummary(c.getString(0));
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }

}
