package com.LupusSolus.ProgressTracker;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;

import com.LupusSolus.ProgressTracker.models.Step;
import com.LupusSolus.ProgressTracker.utils.MyHelper;
import com.LupusSolus.ProgressTracker.utils.TrackDB;
import com.LupusSolus.ProgressTracker.utils.image.BitmapLoader;

import java.util.Collections;
import java.util.List;

public class SlideshowActivityCopy extends Activity {
    private String trackName;
    private List<Step> steps;
    private ViewPager pager;
    private MediaController mPlayback;
    private int index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.slideshow_new);
        trackName = getIntent().getStringExtra("TRACK_NAME");

        try {
            steps = TrackDB.findSteps(getIntent().getIntExtra("TRACK_ID", -1));
            Collections.reverse(steps);
            pager = (ViewPager) findViewById(R.id.pager);
            pager.setAdapter(mAdapter);
            pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int i, float v, int i2) {

                }

                @Override
                public void onPageSelected(int i) {
                    index = i;
                }

                @Override
                public void onPageScrollStateChanged(int i) {

                }
            });
            pager.setCurrentItem(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mPlayback = new MediaController(this, false);
        mPlayback.setAnchorView(findViewById(R.id.pager));
        mPlayback.setPrevNextListeners(new OnClickListener() {
                                           @Override
                                           public void onClick(View view) {
                                               myControl.seekTo((index + 1) * 3 * 1000);

                                           }
                                       }, new OnClickListener() {
                                           @Override
                                           public void onClick(View view) {
                                               myControl.seekTo((index - 1) * 3 * 1000);
                                           }
                                       }
        );
        mPlayback.setMediaPlayer(myControl);
    }

    @Override
    protected void onResume() {
        try {
            mControllerTask.execute();
        } catch (Exception ex) {
            mPlayback.show(0);
        }
        super.onResume();
    }

    PagerAdapter mAdapter = new PagerAdapter() {
        @Override
        public int getCount() {
            return steps.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object o) {

            return view == o;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ImageView image = (ImageView) getLayoutInflater().inflate(R.layout.slideshow_item, container, false);

            DisplayMetrics metrics = new DisplayMetrics();

            getWindowManager().getDefaultDisplay().getMetrics(metrics);

            int width = metrics.widthPixels;
            int height = metrics.heightPixels;

            Bitmap picture = BitmapLoader.sizeAndOrient(MyHelper.TRACKS_ROOT + trackName + "/" + String.valueOf(steps.get(position).getID()) + ".jpg", height, width);
            image.setImageBitmap(picture);

            container.addView(image);

            image.setOnTouchListener(mClickListener);
            return image;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    };

    private AsyncTask<Void, Void, Void> mControllerTask = new AsyncTask<Void, Void, Void>() {
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Thread.sleep(1000);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mPlayback.show(0);
            super.onPostExecute(aVoid);
        }
    };

    MediaController.MediaPlayerControl myControl = new MediaController.MediaPlayerControl() {
        private boolean mPlaying;

        private Thread timerTask;

        @Override
        public void start() {
            mPlaying = true;
            timerTask = new Thread() {
                @Override
                public void run() {
                    while (index < steps.size() - 1 && mPlaying) {
                        try {
                            Thread.sleep(3000);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    pager.setCurrentItem(++index);
                                }
                            });
                        } catch (InterruptedException e) {
                            pause();
                            e.printStackTrace();
                        }

                    }
                }
            };
            timerTask.start();
        }

        @Override
        public void pause() {
            mPlaying = false;
            //if(timerTask != null){
            timerTask.interrupt();
            //}
        }

        @Override
        public int getDuration() {
            return steps.size() * 3 * 1000;
        }

        @Override
        public int getCurrentPosition() {
            return index * 3 * 1000;
        }

        @Override
        public void seekTo(int i) {
            int seconds = i / 1000;
            int pos = seconds / 3;


            if (pos < 0) {
                pos = 0;
            }
            if (pos >= steps.size()) {
                pos = steps.size() - 1;
            }
            pager.setCurrentItem(pos);
            index = pos;
        }

        @Override
        public boolean isPlaying() {
            return mPlaying;
        }

        @Override
        public int getBufferPercentage() {
            return 100;
        }

        @Override
        public boolean canPause() {
            return true;
        }

        @Override
        public boolean canSeekBackward() {
            return true;
        }

        @Override
        public boolean canSeekForward() {
            return true;
        }
    };

    private View.OnTouchListener mClickListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            myControl.pause();
            mPlayback.show(0);
            return false;
        }
    };

}
