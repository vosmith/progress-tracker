package com.LupusSolus.ProgressTracker;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.TextView;

import com.LupusSolus.ProgressTracker.models.Step;
import com.LupusSolus.ProgressTracker.utils.MyHelper;
import com.LupusSolus.ProgressTracker.utils.TrackDB;
import com.LupusSolus.ProgressTracker.utils.image.PhotoLoader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShowAlarmActivity extends Activity implements OnClickListener {
    private PowerManager.WakeLock wl;
    private String trackName;
    private int trackId;
    private Notification n;
    private static Intent intent;
    private static MediaPlayer mp;

    private static final String tag = "ShowAlarm";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.showalarm);
        Log.d(tag, "show alarm reached");
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);


        //Get the track and populate the screen
        TrackDB.open(this);

        if (getIntent().getBooleanExtra("ALARM_NOTIF", false)) {
            wl = ((PowerManager) getSystemService(POWER_SERVICE)).newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "alarm");

            trackName = getIntent().getStringExtra("TRACK_NAME");
            trackId = getIntent().getIntExtra("TRACK_ID", -1);

            Log.d(tag, String.format("Creating link to track %s with id %d", trackName, trackId));

            intent = new Intent(this, AddStepActivity.class);
            intent.putExtra("TRACK_ID", trackId);
            intent.putExtra("TRACK_NAME", trackName);
            intent.putExtra("FROM_ALARM", true);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
            builder.setContentTitle("Time to make tracks!")
                    .setSmallIcon(R.drawable.ic_camera)
                    .setContentIntent(PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_CANCEL_CURRENT))
                    .setAutoCancel(true)
                    .setContentText("Add the next step to your " + trackName + " track.")
                    .setLights(0xFFFF00, 500, 500)
                    .setTicker("Time to make tracks!");

            n = builder.build();

            SharedPreferences globalprefs = PreferenceManager.getDefaultSharedPreferences(this);

            if (globalprefs.getBoolean("alarm_notifications", false)) {
                Log.d(tag, "setting sound");
                String ringtoneUri = globalprefs.getString("alarm_tone", "");

                mp = new MediaPlayer();
                try {
                    if (ringtoneUri.equals(Settings.System.DEFAULT_ALARM_ALERT_URI.toString())) {
                        Log.d("settings", "Setting ringtone to default");

                        mp.setDataSource(this, Uri.parse(ringtoneUri));
                        mp.setAudioStreamType(AudioManager.STREAM_ALARM);
                        mp.prepare();

                    } else {
                        Cursor c = getContentResolver().query(Uri.parse(ringtoneUri), new String[]{MediaStore.MediaColumns.DATA}, null, null, null);

                        if (c != null) {
                            c.moveToFirst();
                            Log.d("settings", String.format("Setting ringtone to %s", c.getString(0)));

                            mp.setDataSource(this, Uri.parse(c.getString(0)));
                            mp.setAudioStreamType(AudioManager.STREAM_ALARM);
                            mp.prepare();
                        }
                    }
                } catch (IOException e) {
                    Log.e(tag, "Error getting ringtone", e);
                    mp = null;
                }


            }

            if (globalprefs.getBoolean("alarm_vibrate", false)) {
                Log.d(tag, "setting vibration");
                n.vibrate = new long[]{0, 500, 500};
            }
        }
        try {
            Step s = TrackDB.findTrack(trackId).getStep(0);

            List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("ID", String.valueOf(R.id.img_alarmPicture));
            map.put("FILE", MyHelper.TRACKS_ROOT + trackName + "/" + String.valueOf(s.getID()) + ".jpg");

            list.add(map);

            PhotoLoader pl = new PhotoLoader(list, this);
            pl.execute(false);

        } catch (Exception e) {
            Log.e(tag, "Error", e);
        }

        ((TextView) findViewById(R.id.lbl_alarmTrackTitle)).setText(trackName);
        ((TextView) findViewById(R.id.lbl_alarmTrackTitle)).setTypeface(MyApplication.font);

        //Set Listeners for the buttons
        findViewById(R.id.btn_confirmAlarm).setOnClickListener(this);
        findViewById(R.id.btn_cancelAlarm).setOnClickListener(this);

        SharedPreferences prefs = getSharedPreferences(trackName, Context.MODE_PRIVATE);
        prefs.edit().putLong("LAST", System.currentTimeMillis()).commit();

        Log.d(tag, "Sending notification");
        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify("show_alarm", 0, n);

        if (mp != null) {
            Log.d(tag, "playing...");
            mp.start();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (mp != null) {
            if (!mp.isPlaying()) {
                Log.d(tag, "playing...");
                mp.start();
            } else {
                Log.d(tag, "resume but already playing...");
            }
        }
        wl.acquire();
        if (killTask.getStatus() == AsyncTask.Status.PENDING) {
            killTask.execute();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_confirmAlarm:

                startActivity(intent);

                finish();
                break;
            case R.id.btn_cancelAlarm:
                finish();
                break;
        }
        killTask.cancel(true);
    }

    @Override
    protected void onPause() {
        wl.release();
        if (mp != null) {
            Log.d(tag, "pausing...");
            mp.pause();
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        Log.d(tag, "Destroying Activity");
        if (mp != null) {
            mp.stop();
            mp.release();
            mp = null;
        }

        super.onDestroy();
    }

    private AsyncTask<Void, Void, Void> killTask = new AsyncTask<Void, Void, Void>() {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Thread.sleep(60 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onCancelled(Void aBoolean) {
            super.onCancelled(aBoolean);
        }

        @Override
        protected void onPostExecute(Void aBoolean) {
            if (!isCancelled()) {
                finish();
            }
            super.onPostExecute(aBoolean);
        }
    };
}
