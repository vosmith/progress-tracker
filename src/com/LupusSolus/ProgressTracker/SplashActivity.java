package com.LupusSolus.ProgressTracker;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;

public class SplashActivity extends Activity implements View.OnTouchListener {
    private static ImageView splash;
    private static SplashTask st;
    private static boolean touched;

    private static final String tag = "Splash";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        splash = (ImageView) findViewById(R.id.splashscreen);
        splash.setVisibility(View.VISIBLE);
        splash.setOnTouchListener(this);

        st = new SplashTask();
        st.execute(null, null);
        touched = false;
    }

    @Override
    protected void onDestroy() {
        st.cancel(true);
        super.onDestroy();    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.splashscreen:
                touched = true;
                st.cancel(true);
                break;

        }
        return false;
    }

    private class SplashTask extends AsyncTask<Void, Void, Void> {
        private AlphaAnimation anim;

        public SplashTask() {
            anim = new AlphaAnimation(1.0f, 0.0f);
            anim.setDuration(1000);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    startActivity(new Intent(getBaseContext(), MainActivity.class));
                    finish();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }
            });
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                Log.e(tag, "Sleep interrupted", e);
            }
            return null;
        }

        @Override
        protected void onCancelled(Void aVoid) {
            Log.d(tag, "Touched is " + (touched ? "true" : "false"));
            Log.d(tag, "Cancelled is " + (isCancelled() ? "true" : "false"));
            if (touched) {
                splash.startAnimation(anim);
                splash.setVisibility(ImageView.GONE);
            }
            super.onCancelled(aVoid);
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.d(tag, "Touched is " + (touched ? "true" : "false"));
            Log.d(tag, "Cancelled is " + (isCancelled() ? "true" : "false"));

            splash.startAnimation(anim);
            splash.setVisibility(ImageView.GONE);

            super.onPostExecute(result);
        }

    }


}