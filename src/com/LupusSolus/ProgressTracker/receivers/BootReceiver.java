package com.LupusSolus.ProgressTracker.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.LupusSolus.ProgressTracker.models.Track;
import com.LupusSolus.ProgressTracker.utils.TrackDB;

/**
 * Created with IntelliJ IDEA.
 * User: vincent
 * Date: 1/9/13
 * Time: 2:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class BootReceiver extends BroadcastReceiver {

    private static final String tag = "BootReceiver";

    public void onReceive(Context context, Intent intent) {
        SharedPreferences prefs;
        TrackDB.open(context);
        try {
            for (Track t : TrackDB.getAllTracks()) {
                Log.v(tag, "Searching for alarm prefs for: " + t.getName());
                prefs = context.getSharedPreferences(t.getName(), Context.MODE_PRIVATE);

                if (prefs != null) {
                    Log.d(tag, "\tFound preferences");
                    if (prefs.getLong("START", 0) > 0) {
                        Log.d(tag, String.format("\tStart time found -> %d", prefs.getLong("START", 0)));

                        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                        Intent i = new Intent(context, AlarmReceiver.class);
                        i.putExtra("TRACK_NAME", t.getName());
                        i.putExtra("TRACK_ID", t.getId());

                        PendingIntent alarm = PendingIntent.getBroadcast(context, t.getId(), i, PendingIntent.FLAG_CANCEL_CURRENT);

                        //Determine repeat frequency
                        long repeatFreq = -1;
                        long start = prefs.getLong("START", 0);
                        long last = prefs.getLong("LAST", 0);

                        Log.d(tag, String.format("Last received: %d", last));
                        String repeat = prefs.getString("REPEAT", "");

                        if (repeat.equals("None") || repeat.equals("")) {
                            repeatFreq = 0;
                        } else if (repeat.equals("15 minutes")) {

                        } else if (repeat.equals("30 minutes")) {

                        } else if (repeat.equals("1 hour")) {
                            repeatFreq = AlarmManager.INTERVAL_HOUR;
                        } else if (repeat.equals("6 hours")) {

                        } else if (repeat.equals("1 day")) {
                            repeatFreq = AlarmManager.INTERVAL_DAY;
                        } else if (repeat.equals("1 week")) {
                            repeatFreq = AlarmManager.INTERVAL_DAY * 7;
                        } else if (repeat.equals("2 weeks")) {

                        } else if (repeat.equals("1 month")) {
                            repeatFreq = AlarmManager.INTERVAL_DAY * 30;
                        }

                        if (repeatFreq > 0) {
                            if (last > 0) {
                                last += repeatFreq;
                            } else {
                                last = start;
                            }
                            Log.d(tag, "Repeating alarm every " + String.valueOf(repeatFreq));
                            am.setRepeating(AlarmManager.RTC_WAKEUP, last, repeatFreq, alarm);
                        } else {
                            if (last == 0) {
                                Log.d(tag, "Non-repeating alarm");
                                am.set(AlarmManager.RTC_WAKEUP, start, alarm);
                            }
                        }
                    }
                }
            }
            TrackDB.close();
        } catch (Exception e) {
            Log.e(tag, "Error opening DB");
        }
    }
}
