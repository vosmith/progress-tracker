package com.LupusSolus.ProgressTracker.receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.LupusSolus.ProgressTracker.AddStepActivity;
import com.LupusSolus.ProgressTracker.R;
import com.LupusSolus.ProgressTracker.ShowAlarmActivity;

/**
 * Created with IntelliJ IDEA.
 * User: vincent
 * Date: 1/11/13
 * Time: 10:04 AM
 * To change this template use File | Settings | File Templates.
 */
public class AlarmReceiver extends BroadcastReceiver {
    private static final String tag = "AlarmRec";

    public void onReceive(Context context, Intent intent) {
        Log.d(tag, "onReceive called");
        SharedPreferences globalprefs = PreferenceManager.getDefaultSharedPreferences(context);
        Notification n;


        Log.d(tag, "building intent");


        if (globalprefs.getBoolean("alarm_notifications", false)) {

            Intent i = new Intent(context, ShowAlarmActivity.class);
            i.putExtra("TRACK_NAME", intent.getStringExtra("TRACK_NAME"));
            i.putExtra("TRACK_ID", intent.getIntExtra("TRACK_ID", -1));
            i.putExtra("ALARM_NOTIF", true);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);

        } else {
            Intent i = new Intent(context, AddStepActivity.class);
            i.putExtra("TRACK_NAME", intent.getStringExtra("TRACK_NAME"));
            i.putExtra("TRACK_ID", intent.getIntExtra("TRACK_ID", -1));
            i.putExtra("FROM_ALARM", true);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
            builder.setContentTitle("Time to make tracks!")
                    .setSmallIcon(R.drawable.ic_camera)
                    .setContentIntent(PendingIntent.getActivity(context, 1, i, PendingIntent.FLAG_CANCEL_CURRENT))
                    .setAutoCancel(true)
                    .setContentText("Add the next step to your " + intent.getStringExtra("TRACK_NAME") + " track.")
                    .setLights(0xFFFF00, 500, 500)
                    .setTicker("Time to make tracks!");

            ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).notify("show_alarm", 0, builder.build());
        }

        //Clear alarm if one time only
        SharedPreferences alarmPrefs = context.getSharedPreferences(intent.getStringExtra("TRACK_NAME"), context.MODE_PRIVATE);
        if (alarmPrefs.getString("REPEAT", "").equals("None")) {
            alarmPrefs.edit().clear().commit();
        }
    }
}
