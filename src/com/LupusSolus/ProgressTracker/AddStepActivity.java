package com.LupusSolus.ProgressTracker;

import android.app.ActionBar;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.LupusSolus.ProgressTracker.datatypes.GeneralData;
import com.LupusSolus.ProgressTracker.datatypes.StepData;
import com.LupusSolus.ProgressTracker.datatypes.WeightLossData;
import com.LupusSolus.ProgressTracker.models.Step;
import com.LupusSolus.ProgressTracker.utils.MyHelper;
import com.LupusSolus.ProgressTracker.utils.TrackDB;
import com.LupusSolus.ProgressTracker.utils.image.BitmapLoader;
import com.LupusSolus.ProgressTracker.utils.image.BitmapLoaderManager;
import com.LupusSolus.ProgressTracker.utils.image.PhotoLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddStepActivity extends FragmentActivity implements OnClickListener {
    private int trackId;
    private String trackName;
    private int stepId;
    private ImageButton btnPhoto;
    // private EditText txtDescr;
    private LinearLayout dataContainer;

    private boolean killTmp = true;
    private boolean picTaken = false;
    private boolean fromAlarm = false;
    private boolean editmode = false;

    private Step currentStep = null;

    private final static String tag = "AddStep";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.addstep);

        btnPhoto = (ImageButton) findViewById(R.id.btn_stepPic);
        // txtDescr = (EditText) findViewById(R.id.txt_notes);
        final Button btnDone = (Button) findViewById(R.id.btn_stepDone);
        dataContainer = (LinearLayout) findViewById(R.id.data_container);

        trackId = getIntent().getIntExtra("TRACK_ID", -1);
        trackName = getIntent().getStringExtra("TRACK_NAME");
        stepId = getIntent().getIntExtra("STEP_ID", -1);
        picTaken = getIntent().getBooleanExtra("PIC_TAKEN", false);
        fromAlarm = getIntent().getBooleanExtra("FROM_ALARM", false);
        editmode = getIntent().getBooleanExtra("EDIT_MODE", false);

        if (savedInstanceState != null) {
            picTaken = savedInstanceState.getBoolean("PIC_TAKEN");
        }

        if (picTaken) {
            new BitmapLoader(this, MyHelper.TEMP_ROOT + "image.jpg", btnPhoto).startLoading();
        } else if (editmode) {
            new BitmapLoader(this, MyHelper.TRACKS_ROOT + trackName + "/" + String.valueOf(stepId) + ".jpg", btnPhoto).startLoading();
        } else {
            btnPhoto.setOnClickListener(this);
        }
        try {
            generateForm(TrackDB.findTrack(trackId).getType());
            if (editmode) {
                fillStepInfo();
                setTitle("Edit step");
            }
        } catch (Exception e) {
            Log.e(tag, "Error generating form", e);
        }
        ActionBar ab = getActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }

        if (btnDone != null) {
            btnDone.setOnClickListener(this);
        }

        ((NotificationManager) getSystemService(NOTIFICATION_SERVICE)).cancel("show_alarm", 0);
        setTitle(trackName);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_stepPic:
                Intent i = new Intent(this, CameraActivity.class);
                startActivityForResult(i, 1);
                break;
            case R.id.btn_stepDone:
                stepDone();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.addstepmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = new Intent(this, ViewTrackActivity.class);
                upIntent.putExtra("TRACK_ID", trackId);

                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is not part of the application's task, so create a new task
                    // with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            .addNextIntent(new Intent(this, MainActivity.class))
                            .addNextIntent(upIntent)
                            .startActivities();
                    finish();
                } else {
                    // This activity is part of the application's task, so simply
                    // navigate up to the hierarchical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                break;
            case R.id.menu_addstep:
                stepDone();
                break;
            case R.id.menu_cancel:
                finish();
                break;
        }
        return true;
    }

    private void stepDone() {
        if (picTaken) {
            // String descr = txtDescr.getText().toString();
            try {
                int type = TrackDB.findTrack(trackId).getType();
                Step s = new Step(trackId, type, buildStepData(type));
                int sId = TrackDB.createStep(s);
                if (sId > 0) {
                    File nextStep = new File(MyHelper.TEMP_ROOT + "image.jpg");
                    if (nextStep.exists()) {
                        nextStep.renameTo(new File(MyHelper.TRACKS_ROOT + trackName + "/" + String.valueOf(sId) + ".jpg"));
                    }
                    Toast.makeText(this, "Step created!", Toast.LENGTH_SHORT).show();

                    //cache new image
                    MyHelper.bitmapCache.put(trackName, BitmapLoaderManager.sizeAndOrient((MyHelper.TRACKS_ROOT + trackName + "/" + String.valueOf(sId) + ".jpg"), 136, 136));

                    if (fromAlarm) {
                        Intent i1 = new Intent(this, ViewTrackActivity.class);
                        i1.putExtra("TRACK_ID", trackId);
                        startActivity(i1);
                    } else {
                        setResult(ViewTrackActivity.NEW_STEP_SUCCESS);
                    }

                    finish();
                } else {
                    Toast.makeText(this, "Error creating step", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception ex) {
                Toast.makeText(this, "An error occurred while creating your step", Toast.LENGTH_LONG).show();
                Log.e(tag, ex.toString(), ex);
            }
        } else if (editmode) {
            try {
                //int type = TrackDB.findTrack(trackId).getType();
                //Step s = new Step(trackId, type, buildStepData(type));
                //int sId = TrackDB.createStep(s);

                currentStep.setNotes(buildStepData(currentStep.getType()));

                if (currentStep.save()) {
                    //    File nextStep = new File(MyHelper.TEMP_ROOT + "image.jpg");
                    //    if (nextStep.exists()) {
                    //        nextStep.renameTo(new File(MyHelper.TRACKS_ROOT + trackName + "/" + String.valueOf(sId) + ".jpg"));
                    //    }
                    Toast.makeText(this, "Changes Saved!", Toast.LENGTH_SHORT).show();

                    //    if(fromAlarm){
                    //        Intent i1 = new Intent(this, ViewTrackActivity.class);
                    //        i1.putExtra("TRACK_ID", trackId);
                    //        startActivity(i1);
                    //    }else{
                    setResult(ViewTrackActivity.NEW_STEP_SUCCESS);
                    //    }

                    finish();
                } else {
                    Toast.makeText(this, "Error creating step", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception ex) {
                Toast.makeText(this, "An error occurred while creating your step", Toast.LENGTH_LONG).show();
                Log.e(tag, ex.toString(), ex);
            }
        } else {
            Toast.makeText(this, "You must take a picture", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    Map<String, Object> hm = new HashMap<String, Object>();
                    List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

                    hm.put("FILE", MyHelper.TEMP_ROOT + "image.jpg");
                    hm.put("ID", String.valueOf(btnPhoto.getId()));

                    list.add(hm);

                    PhotoLoader pl = new PhotoLoader(list, this);
                    pl.execute(false);

                    //BitmapFactory.Options opts = new BitmapFactory.Options();
                    //opts.inSampleSize = 8;
                    //btnPhoto.setImageBitmap(BitmapFactory.decodeFile(MyHelper.TEMP_ROOT + "image.jpg", opts));
                    picTaken = true;
                } else if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(this, "Camera cancelled", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "Camera error", Toast.LENGTH_LONG).show();
                }
                break;
            default:
                Toast.makeText(this, "request code not recognized", Toast.LENGTH_LONG).show();
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        killTmp = false;
        outState.putBoolean("PIC_TAKEN", picTaken);

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        if (killTmp) {
            File tmp = new File(MyHelper.TEMP_ROOT + "image.jpg");
            if (tmp.exists()) {
                tmp.delete();
            }
        } else {
            killTmp = true;
        }
        super.onDestroy();
    }

    private void generateForm(int type) {
        JSONObject format = null;

        switch (type) {
            case StepData.TYPE_GENERAL:
                try {
                    format = GeneralData.getFormat();
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
                break;
            case StepData.TYPE_WEIGHT_LOSS:
                try {
                    format = WeightLossData.getFormat();
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
        }

        if (format != null) {
            // find labels
            JSONArray names = format.names();

            for (int i = 0; i < names.length(); i++) {
                TextView label = new TextView(this);
                View field = null;

                try {
                    Object dataType = format.get(names.getString(i));
                    if (dataType.equals("string")) {
                        field = new EditText(this);
                        ((EditText) field).setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

                    } else if (dataType.equals("float")) {
                        field = new EditText(this);
                        ((EditText) field).setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
                    } else if (dataType.equals("integer")) {
                        field = new EditText(this);
                        ((EditText) field).setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
                    } else if (dataType.getClass().equals(JSONObject.class)) {
                        field = new LinearLayout(this);
                        ((LinearLayout) field).setOrientation(LinearLayout.HORIZONTAL);

                        View valueField = null;
                        TextView unitLabel = new TextView(this);
                        unitLabel.setText("unit:");
                        EditText unitField = new EditText(this);

                        String valueDataType = ((JSONObject) dataType).getString("value");

                        if (valueDataType.equals("string")) {
                            valueField = new EditText(this);
                            ((EditText) valueField).setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                        } else if (valueDataType.equals("float")) {
                            valueField = new EditText(this);
                            ((EditText) valueField).setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
                        } else if (valueDataType.equals("integer")) {
                            valueField = new EditText(this);
                            ((EditText) valueField).setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
                        } else {
                            valueField = new TextView(this);
                            ((TextView) valueField).setText("Data type: " + valueDataType + " not supported.");
                        }
                        unitField.setTag("unit");
                        valueField.setTag("value");

                        ((LinearLayout) field).addView(valueField);
                        ((LinearLayout) field).addView(unitLabel);
                        ((LinearLayout) field).addView(unitField);

                    } else {
                        field = new TextView(this);
                        ((TextView) field).setText("Data type: " + dataType + " not supported.");
                    }
                    label.setText(names.getString(i) + ": ");
                    field.setTag(names.getString(i));
                    dataContainer.addView(field, 0);
                    dataContainer.addView(label, 0);
                } catch (JSONException e) {
                    Toast.makeText(this, "Error creating form", Toast.LENGTH_SHORT).show();
                    Log.e(tag, "Error creating form", e);
                }
            }
        }

    }

    private void fillStepInfo() {
        try {
            currentStep = TrackDB.findTrack(trackId).findStepById(stepId);
            JSONObject jsonData = new JSONObject(currentStep.getNotes().toString());

            JSONArray names = jsonData.names();

            for (int i = 0; i < names.length(); i++) {
                Log.d(tag, "Searching for View with tag: " + names.get(i));
                EditText v = (EditText) dataContainer.findViewWithTag(names.get(i));
                v.setText((String) jsonData.get((String) names.get(i)));
                Log.d(tag, "Set view text to: " + (jsonData.get((String) names.get(i))));
            }

        } catch (Exception e) {
            Log.e(tag, "Error finding step");
        }
    }

    private StepData buildStepData(int type) {
        JSONObject format = null;
        StepData retObj = null;

        switch (type) {
            case StepData.TYPE_GENERAL:
                try {
                    format = GeneralData.getFormat();
                } catch (JSONException e1) {
                    Log.e(tag, "Error in buildStepData", e1);
                }
                break;
            case StepData.TYPE_WEIGHT_LOSS:
                try {
                    format = WeightLossData.getFormat();
                } catch (JSONException e1) {
                    Log.e(tag, "Error in buildStepData", e1);
                }
        }

        if (format != null) {

            // find views
            JSONArray names = format.names();
            JSONObject data = new JSONObject();

            for (int i = 0; i < names.length(); i++) {
                try {
                    View value = dataContainer.findViewWithTag(names.getString(i));
                    if (value != null) {

                        Object dataType = format.get(names.getString(i));
                        if (dataType.equals("string")) {
                            data.put(names.getString(i), ((EditText) value).getText().toString());
                        } else if (dataType.equals("float")) {
                            data.put(names.getString(i), Double.parseDouble(((EditText) value).getText().toString()));
                        } else if (dataType.equals("integer")) {
                            data.put(names.getString(i), Integer.parseInt(((EditText) value).getText().toString()));
                        } else if (dataType.getClass() == JSONObject.class) {
                            JSONObject scalar = new JSONObject();

                            if (((JSONObject) dataType).getString("value").equals("string")) {
                                scalar.put("value", ((EditText) ((LinearLayout) value).findViewWithTag("value")).getText().toString());
                            } else if (((JSONObject) dataType).getString("value").equals("float")) {
                                scalar.put("value", Double.parseDouble(((EditText) ((LinearLayout) value).findViewWithTag("value")).getText().toString()));
                            } else if (((JSONObject) dataType).getString("value").equals("integer")) {
                                scalar.put("value", Integer.parseInt(((EditText) ((LinearLayout) value).findViewWithTag("value")).getText().toString()));
                            } else {
                                Log.d("build Step", "Data type " + dataType + " not supported.");
                            }

                            scalar.put("unit", ((EditText) ((LinearLayout) value).findViewWithTag("unit")).getText().toString());

                            data.put(names.getString(i), scalar);
                        } else {
                            Log.d("build Step", "Data type " + dataType + " not supported.");
                        }
                    }
                } catch (JSONException e) {
                    try {
                        data.put(names.getString(i), null);
                    } catch (JSONException e1) {
                        Log.e(tag, "Error in buildStepData", e1);
                    }
                    Log.e(tag, "Error in buildStepData", e);
                }
            }
            switch (type) {
                case StepData.TYPE_GENERAL:
                    try {
                        retObj = new GeneralData(data);
                        retObj.verifyFormat();
                    } catch (JSONException e) {
                        Log.e(tag, "Error in buildStepData", e);
                    }

                    break;
                case StepData.TYPE_WEIGHT_LOSS:
                    try {
                        retObj = new WeightLossData(data);
                        retObj.verifyFormat();
                    } catch (JSONException e) {
                        Log.e(tag, "Error in buildStepData", e);
                    }
            }
        }
        Log.d(tag, "Return object: " + retObj.toString());
        return retObj;
    }
}
