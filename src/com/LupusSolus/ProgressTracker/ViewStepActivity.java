package com.LupusSolus.ProgressTracker;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.LupusSolus.ProgressTracker.models.Track;
import com.LupusSolus.ProgressTracker.utils.MyHelper;
import com.LupusSolus.ProgressTracker.utils.TrackDB;
import com.LupusSolus.ProgressTracker.utils.image.PhotoLoader;

public class ViewStepActivity extends Activity implements View.OnTouchListener {
    private Track t;
    private ScaleGestureDetector scaleDetector;
    private GestureDetectorCompat translateDetector;
    private DisplayMetrics mMetrics = new DisplayMetrics();
    private static ImageView firstImg;
    private static Matrix m;

    private static final String tag = "ViewStep";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.viewsteps);
        m = new Matrix();

        getWindowManager().getDefaultDisplay().getMetrics(mMetrics);

        firstImg = (ImageView) findViewById(R.id.steps_mainImg);
        TextView stepDescr = (TextView) findViewById(R.id.txt_stepDescr);
        TextView stepDate = (TextView) findViewById(R.id.txt_stepDate);
        try {
            t = TrackDB.findTrack(getIntent().getIntExtra("TRACK_ID", -1));
            int sID = getIntent().getIntExtra("STEP_ID", 0);

            stepDate.setText(t.findStepById(sID).getCreated().toLocaleString());

            if (!t.findStepById(sID).getNotes().equals(null)) {
                stepDescr.setText(t.findStepById(sID).getNotes().toListString());
            } else {
                ((RelativeLayout) stepDescr.getParent()).removeView(stepDescr);
            }

            Bitmap b = PhotoLoader.sizeAndOrient(MyHelper.TRACKS_ROOT + t.getName() + "/" + String.valueOf(sID) + ".jpg", mMetrics.widthPixels, mMetrics.heightPixels);

            firstImg.setOnTouchListener(this);
            m.postTranslate(((0.5f) * mMetrics.widthPixels) - ((0.5f) * b.getWidth()), ((0.5f) * mMetrics.heightPixels) - ((0.5f) * b.getHeight()));
            firstImg.setImageMatrix(m);
            firstImg.setImageBitmap(b);

            scaleDetector = new ScaleGestureDetector(this, new MyScaleListener());
            translateDetector = new GestureDetectorCompat(this, new MyTranslateListener());
        } catch (Exception e) {
            Log.e(tag, "Error retreiving track", e);
        }
        stepDate.setTypeface(MyApplication.boldFont);
        stepDescr.setTypeface(MyApplication.boldFont);

        stepDescr.setSelected(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getPointerCount() > 1) {
            scaleDetector.onTouchEvent(event);
        } else {
            translateDetector.onTouchEvent(event);
        }
        return true;
    }

    private class MyScaleListener implements ScaleGestureDetector.OnScaleGestureListener {
        float scale = 1.0f;

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            Log.i(tag, "OnScale");
            Log.i(tag, String.format("Scale Factor: %f", detector.getScaleFactor()));

            //m.reset();
            scale = detector.getScaleFactor();

            m.postScale(scale, scale, detector.getFocusX(), detector.getFocusY());

            //m.postScale(5.0f, 5.0f);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    firstImg.setImageMatrix(m);
                }
            });

            return true;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            Log.i(tag, "Scale Begin");
            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            Log.i(tag, "Scale End");
        }
    }

    private class MyTranslateListener extends GestureDetector.SimpleOnGestureListener {
        float x = 0.0f;
        float y = 0.0f;

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            Log.i(tag, String.format("Scrolling %fx : %fy", distanceX, distanceY));

            x = distanceX;
            y = distanceY;

            m.postTranslate((-1) * x, (-1) * y);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    firstImg.setImageMatrix(m);
                }
            });

            return super.onScroll(e1, e2, distanceX, distanceY);
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            Log.i(tag, "SingleTap");
            return super.onSingleTapUp(e);
        }
    }
}
