package com.LupusSolus.ProgressTracker;

import android.app.Application;
import android.graphics.Typeface;

import com.LupusSolus.ProgressTracker.utils.CrashDBSender;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

/**
 * Created with IntelliJ IDEA.
 * User: vincent
 * Date: 2/6/13
 * Time: 8:57 PM
 * To change this template use File | Settings | File Templates.
 */
@ReportsCrashes(formKey = "", // will not be used
        mode = ReportingInteractionMode.DIALOG,
        resToastText = R.string.error_toast)
public class MyApplication extends Application {
    public static Typeface font;
    public static Typeface boldFont;

    @Override
    public void onCreate() {
        font = Typeface.createFromAsset(getAssets(), "BRI293.TTF");
        boldFont = Typeface.createFromAsset(getAssets(), "brie-medium.ttf");

        ACRA.getConfig().setSendReportsInDevMode(true);
        ACRA.init(this);
        CrashDBSender yourSender = new CrashDBSender();
        ACRA.getErrorReporter().setReportSender(yourSender);
        super.onCreate();
    }
}
