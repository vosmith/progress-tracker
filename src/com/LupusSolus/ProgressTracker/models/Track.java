package com.LupusSolus.ProgressTracker.models;

import android.util.Log;

import com.LupusSolus.ProgressTracker.datatypes.StepData;
import com.LupusSolus.ProgressTracker.utils.TrackDB;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Track {
    private int _id;
    private String _name;
    private String _description;
    private final Date _created;
    private Date _modified;
    private List<Step> _steps;
    private int _type;
    private static final String tag = "Track";

    public Track(String name, int type) throws NullPointerException {
        if (name.equals("")) {
            throw new NullPointerException("name cannot be null");
        } else {
            _id = -1;
            _name = name;
            _type = type;
            _created = Calendar.getInstance().getTime();
        }
    }

    public Track(String name, String descr, int type) throws NullPointerException {
        if (name.equals("")) {
            throw new NullPointerException("name cannot be null");
        } else {
            _id = -1;
            _name = name;
            _description = descr;
            _type = type;
            _created = Calendar.getInstance().getTime();
        }
    }

    public Track(Integer id, String name, String descr, int type, Date created, Date modified) throws NullPointerException {
        if (id.equals(null)) {
            throw new NullPointerException("id cannot be null");
        } else if (name.equals("")) {
            throw new NullPointerException("name cannot be null");
        } else if (created == null) {
            throw new NullPointerException("created date cannot be null");
        } else {
            _id = id;
            _name = name;
            _description = descr;
            _created = created;
            _modified = modified;
            _type = type;
            Log.v(tag, String.format("Building Track object for %s, id %d", _name, _id));
            findSteps();
        }
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) throws NullPointerException {
        if (name.equals("")) {
            throw new NullPointerException("Name cannot be null");
        } else {
            _name = name;
        }
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;
    }

    public int getType() {
        return _type;
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        if (_id == -1) {
            _id = id;
        }
    }

    public Date getCreated() {
        return _created;
    }

    public Date getModified() {
        return _modified;
    }

    public void save() {
        try {
            _modified = Calendar.getInstance().getTime();
            TrackDB.saveTrack(this);
        } catch (Exception e) {
            Log.e(tag, "Error saving Track", e);
        }
    }

    public int makeStep(StepData notes) {
        try {
            Step s = new Step(_id, _type, notes);
            int step_id = TrackDB.createStep(s);

            if (step_id > 0) {
                findSteps();
            }
            return step_id;
        } catch (Exception e) {
            Log.e(tag, "Error saving Track", e);
            return -1;
        }
    }

    private void findSteps() {
        try {
            _steps = TrackDB.findSteps(_id);
        } catch (Exception e) {
            Log.e(tag, "", e);
        }
    }

    public List<Step> getAllSteps() {
        findSteps();
        Log.d(tag, "found " + String.valueOf(_steps.size()) + " steps");
        return _steps;
    }

    public Step getStep(int index) {
        Step ret = null;
        try {
            ret = _steps.get(index);
        } catch (IndexOutOfBoundsException e) {
            Log.e(tag, "", e);
        }
        return ret;
    }

    public Step findStepById(int id) {
        for (Step s : _steps) {
            if (id == s.getID()) {
                return s;
            }
        }
        return null;
    }

    public int getStepCount() {
        if (_steps != null) {
            return _steps.size();
        }
        return 0;
    }

    @Override
    public String toString() {
        return _name;
    }

}
