package com.LupusSolus.ProgressTracker.models;

import android.util.Log;

import com.LupusSolus.ProgressTracker.datatypes.StepData;
import com.LupusSolus.ProgressTracker.utils.TrackDB;

import java.util.Calendar;
import java.util.Date;

public class Step {
    private int _id;
    private int _trackId;
    private int _type;
    private StepData _notes;
    private Date _created;


    private static final String tag = "Step";

    public Step(int trackId, int type, StepData notes) {
        _trackId = trackId;
        _notes = notes;
        _type = type;
        _created = Calendar.getInstance().getTime();
    }

    public Step(int id, int trackId, int type, StepData notes, Date created) {
        _id = id;
        _trackId = trackId;
        _type = type;
        _notes = notes;
        _created = created;
    }

    public int getID() {
        return _id;
    }

    public Track getTrack() {
        Track retTrack = null;
        try {
            retTrack = TrackDB.findTrack(_trackId);
        } catch (Exception e) {
            Log.e(tag, "Could not find track", e);
        }
        return retTrack;
    }

    public StepData getNotes() {
        return _notes;
    }

    public void setNotes(StepData notes) {
        _notes = notes;
    }

    public int getType() {
        return _type;
    }

    public Date getCreated() {
        return _created;
    }

    public boolean save() {
        try {
            return TrackDB.editStep(this);
        } catch (Exception e) {
            Log.e(tag, "Error saving info", e);
        }
        return false;
    }
}
