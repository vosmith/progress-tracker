package com.LupusSolus.ProgressTracker;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.LupusSolus.ProgressTracker.dialogs.ConfimDeleteDialog;
import com.LupusSolus.ProgressTracker.models.Track;
import com.LupusSolus.ProgressTracker.utils.MyHelper;
import com.LupusSolus.ProgressTracker.utils.TrackDB;
import com.LupusSolus.ProgressTracker.utils.image.BitmapCallbacks;
import com.LupusSolus.ProgressTracker.utils.image.BitmapLoaderManager;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: vincent
 * Date: 1/18/13
 * Time: 8:17 AM
 * To change this template use File | Settings | File Templates.
 */
public class MainListFragment extends Fragment implements AdapterView.OnItemClickListener {
    private static TrackListAdapter aa;
    private static final String tag = "MainList";
    private static View mainView;
    private Object fail;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int orientation = getResources().getConfiguration().orientation;
        LinearLayout ll;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            ll = (LinearLayout) inflater.inflate(R.layout.main_list, null);
        } else {
            ll = (LinearLayout) inflater.inflate(R.layout.main_grid, null);
        }

        ((AdapterView) ll.findViewById(R.id.list)).setEmptyView(inflater.inflate(R.layout.empty, null));

        return ll;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mainView = view;
        getListView().setOnItemClickListener(this);
        registerForContextMenu(getListView());
    }

    @Override
    public void onResume() {
        try {
            aa = new TrackListAdapter(getActivity(), TrackDB.getAllTracks());
            getListView().setAdapter(aa);
        } catch (Exception e) {
            Log.e(tag, "Error fetching tracks", e);
        }
        super.onResume();
    }

    private AdapterView getListView() {
        return (AdapterView) mainView.findViewById(R.id.list);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent i = new Intent(getActivity(), ViewTrackActivity.class);
        i.putExtra("TRACK_ID", aa.getItem(position).getId());
        startActivity(i);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater mi = getActivity().getMenuInflater();
        mi.inflate(R.menu.tracklistcontextmenu, menu);

        MainActivity.selected = aa.getItem(((AdapterView.AdapterContextMenuInfo) menuInfo).position);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ctx_open:
                Intent i = new Intent(getActivity(), ViewTrackActivity.class);
                i.putExtra("TRACK_ID", MainActivity.selected.getId());
                startActivity(i);
                break;
            case R.id.ctx_delete:

                new ConfimDeleteDialog().show(getFragmentManager(), "confirm_delete");
                break;
        }
        return true;
        //return super.onContextItemSelected(item);
    }

    private class TrackListAdapter extends BaseAdapter {
        private List<Track> list;
        private Context mCtx;

        public TrackListAdapter(Context ctx, List<Track> trackList) {
            list = trackList;

            Log.d(tag, "Creating tracklist adapter");
            Log.v(tag, "List contains the following tracks");
            for (Track track : list) {
                Log.v(tag, String.format("\tName: %s, ID: %d", track.getName(), track.getId()));
            }

            mCtx = ctx;
        }

        @Override
        public int getItemViewType(int position) {
            return -1;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Track getItem(int arg0) {
            return list.get(arg0);
        }

        @Override
        public long getItemId(int position) {
            return list.get(position).getId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Log.v(tag, String.format("Building view for Track at position %d", position));
            Track t = list.get(position);
            if (convertView == null) {
                Log.d(tag, String.format("Creating view for track %s, id: %d", t.getName(), t.getId()));
                convertView = ((LayoutInflater) mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.alltrackslistitem, null);
            } else {

                Log.d(tag, "Recycling view from " + ((TextView) convertView.findViewById(R.id.tracklist_name)).getText().toString());
            }
            try {
                ImageView img = ((ImageView) convertView.findViewById(R.id.tracklist_img));
                img.setTag(t.getName());
                Bitmap b = MyHelper.bitmapCache.get(t.getName());
                if (b == null) {
                    Log.d(tag, "No image found in cache");
                    if (false) {
                        b = BitmapLoaderManager.sizeAndOrient(MyHelper.TRACKS_ROOT + t.getName() + "/" + t.getStep(0).getID() + ".jpg", 136, 136);
                        img.setImageBitmap(b);
                        MyHelper.bitmapCache.put(t.getName(), b);
                    } else {
                        Bundle bundle = new Bundle();
                        bundle.putString("TAG", t.getName());
                        bundle.putString("FILE", MyHelper.TRACKS_ROOT + t.getName() + "/" + t.getStep(0).getID() + ".jpg");
                        bundle.putSerializable("ANIM", BitmapLoaderManager.Animator.FLY_IN_LEFT_SIDE);
                        bundle.putBoolean("CACHE", true);

                        Loader task = ((FragmentActivity) mCtx).getSupportLoaderManager().initLoader(t.getId(), bundle, new BitmapCallbacks(mCtx, ((FragmentActivity) mCtx).findViewById(R.id.list)));

                        task.startLoading();
                    }
                } else {
                    Log.d(tag, "using cached image");
                    TranslateAnimation ta = new TranslateAnimation(Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.ABSOLUTE, 0, Animation.ABSOLUTE, 0);
                    ta.setDuration(200);
                    img.setAnimation(ta);
                    img.setImageBitmap(b);
                }
            } catch (Exception e) {
                Log.e(tag, "error loading photo", e);
            }

            if (MainActivity.deleteMode) {
                ImageView iv = ((ImageView) convertView.findViewById(R.id.tracklist_delete));
                iv.setVisibility(View.VISIBLE);
                iv.setOnClickListener(MainActivity.ocl);
                iv.setTag(t.getId());
            }

            TextView txtName = (TextView) convertView.findViewById(R.id.tracklist_name);

            txtName.setTypeface(MyApplication.font);
            txtName.setText(t.getName());
            try {
                ((TextView) convertView.findViewById(R.id.tracklist_date)).setText("Last Updated: " + t.getStep(0).getCreated().toLocaleString());
            } catch (NullPointerException e) {
                ((TextView) convertView.findViewById(R.id.tracklist_date)).setText("Last Updated: Unknown");
            }

            if (mCtx.getSharedPreferences(t.getName(), Context.MODE_PRIVATE).getLong("START", 0) > 0) {
                convertView.findViewById(R.id.img_alarm).setVisibility(View.VISIBLE);
            } else {
                convertView.findViewById(R.id.img_alarm).setVisibility(View.GONE);
            }
            return convertView;
        }

    }
}
