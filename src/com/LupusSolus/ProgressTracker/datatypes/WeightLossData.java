package com.LupusSolus.ProgressTracker.datatypes;

import org.json.JSONException;
import org.json.JSONObject;

public class WeightLossData extends StepData {
    private static JSONObject format;

    public WeightLossData(JSONObject data) throws JSONException {
        _data = data;
        fmt = format;
    }

    @Override
    public String toListString() {
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("Weight: ").append(_data.getJSONObject("weight").getDouble("value")).append(" ").append(_data.getJSONObject("weight").getString("unit")).append("\n");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            sb.append("Waist size: ").append(_data.getJSONObject("waist").getDouble("value")).append(" ").append(_data.getJSONObject("waist").getString("unit")).append("\n");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            sb.append("BMI: ").append(_data.getInt("bmi")).append("\n");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            sb.append("Notes: ").append(_data.getString("notes")).append("\n");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return sb.toString();
    }

    public static JSONObject getFormat() throws JSONException {
        if (format == null) {
            format = new JSONObject("{weight:{value:\"float\",unit:\"string\"},waist:{value:\"float\",unit:\"string\"}, bmi:\"integer\", notes:\"string\"}");

        }
        return format;
    }
}
