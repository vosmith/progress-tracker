package com.LupusSolus.ProgressTracker.datatypes;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class StepData {
    public static final int MISSING_PARAMETERS = -1;
    public static final int EXTRA_PARAMETERS = -2;
    public static final int UNKNOWN_TYPE = -3;

    public static final int TYPE_GENERAL = 1;
    public static final int TYPE_WEIGHT_LOSS = 2;
    public static final int TYPE_PLANT_GROWTH = 3;
    public static final int TYPE_MOVEMENT = 4;

    private static final String tag = "StepData";
    private boolean verified = false;

    protected JSONObject fmt;
    protected JSONObject _data;

    public abstract String toListString();

    public int verifyFormat() {
        int status = 0;
        JSONArray names = fmt.names();

        for (int i = 0; i < names.length(); i++) {
            try {
                Object dataType = fmt.get(names.getString(i));
                if (dataType.equals("string")) {
                    if (_data.optString((String) names.get(i)) == "") {
                        status = MISSING_PARAMETERS;
                        break;
                    }
                } else if (dataType.equals("float") || dataType.equals("double")) {
                    if (_data.getDouble((String) names.get(i)) == 0.0) {
                        //just a check
                    }
                } else if (dataType.equals("integer")) {
                    if (_data.getInt((String) names.get(i)) == 0) {
                        //just a check
                    }
                } else if (dataType.getClass() == JSONObject.class) {
                    JSONObject scalar = new JSONObject();

                    if (((JSONObject) dataType).getString("value").equals("string")) {
                        if (((JSONObject) _data.get((String) names.get(i))).optString("value") == "") {
                            status = MISSING_PARAMETERS;
                            break;
                        }
                    } else if (((JSONObject) dataType).getString("value").equals("float") || ((JSONObject) dataType).getString("value").equals("double")) {
                        if (((JSONObject) _data.get((String) names.get(i))).getDouble("value") == 0.0) {
                            //just a check
                        }
                    } else if (((JSONObject) dataType).getString("value").equals("integer")) {
                        if (((JSONObject) _data.get((String) names.get(i))).getInt("value") == 0) {
                            //just a check
                        }
                    } else {
                        status = UNKNOWN_TYPE;
                        Log.d(tag, "Data type " + dataType + " not supported.");
                    }

                } else {
                    status = UNKNOWN_TYPE;
                    Log.d(tag, "Data type " + dataType + " not supported.");
                }
            } catch (JSONException e) {
                status = MISSING_PARAMETERS;
                Log.e(tag, "error verifying data", e);
            }
        }

        if (status == 0) {
            if (names.length() != _data.names().length()) {
                status = EXTRA_PARAMETERS;
            }
        }

        if (status == 0) {
            verified = true;
        }
        Log.d(tag, "Verification status is: " + String.valueOf(status));
        return status;
    }

    public String toString() {
        try {
            return _data.toString(2);
        } catch (JSONException e) {
            e.printStackTrace();
            return "error creating description";
        }
    }

    public boolean isVerified() {
        return verified;
    }
}
