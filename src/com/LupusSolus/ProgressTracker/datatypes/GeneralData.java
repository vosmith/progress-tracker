package com.LupusSolus.ProgressTracker.datatypes;

import org.json.JSONException;
import org.json.JSONObject;

public class GeneralData extends StepData {
    private static JSONObject format;

    public GeneralData(JSONObject data) throws JSONException {
        _data = data;
        fmt = format;
    }

    @Override
    public String toListString() {
        try {
            return "Notes: " + _data.getString("notes");
        } catch (JSONException e) {
            e.printStackTrace();
            return "error creating description";
        }
    }

    public static JSONObject getFormat() throws JSONException {
        if (format == null) {
            format = new JSONObject("{notes:\"string\"}");
        }
        return format;
    }
}
