package com.LupusSolus.ProgressTracker;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.LupusSolus.ProgressTracker.models.Step;
import com.LupusSolus.ProgressTracker.utils.MyHelper;
import com.LupusSolus.ProgressTracker.utils.TrackDB;
import com.LupusSolus.ProgressTracker.utils.image.PhotoLoader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SlideshowActivity extends Activity implements OnClickListener, OnTouchListener {
    private ImageView currentPic;
    private RelativeLayout playback;
    private ImageView btnPlay;
    private ImageView btnEnd;
    private ImageView btnBeginning;
    private GestureDetector gd;
    private static String trackName;
    private static int pos;
    private static PhotoLoader pl;
    private static List<Step> steps;
    private static Bitmap previous;
    private static Bitmap next;
    private static Handler playHandler;
    private static Thread playThread;
    private static boolean play;
    private static boolean ready;

    private static final String tag = "SlideShowActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.slideshow);

        currentPic = (ImageView) findViewById(R.id.img_currentPic);
        playback = (RelativeLayout) findViewById(R.id.img_playback);
        btnPlay = (ImageView) findViewById(R.id.btn_play);
        btnEnd = (ImageView) findViewById(R.id.btn_end);
        btnBeginning = (ImageView) findViewById(R.id.btn_beginning);

        trackName = getIntent().getStringExtra("TRACK_NAME");

        try {
            steps = TrackDB.findSteps(getIntent().getIntExtra("TRACK_ID", -1));
            Collections.reverse(steps);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //set initial position for display
        pos = 0;
        play = true;
        ready = false;

        gd = new GestureDetector(this, new MyGestureListener());

        currentPic.setOnTouchListener(this);
        playback.setOnClickListener(this);
        btnPlay.setOnClickListener(this);
        btnEnd.setOnClickListener(this);
        btnBeginning.setOnClickListener(this);

        playHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        goNext();
                }
            }
        };

        playThread = new Thread() {
            @Override
            public void run() {
                while (play) {
                    if (ready) {
                        try {
                            Thread.sleep(2000);
                            if (play) {
                                ready = false;
                                Message m = playHandler.obtainMessage(1);
                                m.sendToTarget();
                            }
                        } catch (Exception e) {
                            Log.e(tag, "error in playThread", e);
                        }
                    } else {
                        Log.i(tag, "not ready");
                    }
                }
            }
        };

        loadImage(0);


        super.onCreate(savedInstanceState);


    }

    @Override
    public void onResume() {
        play = true;
        ready = true;
        super.onResume();
    }

    private void loadImage(int index) {
        play = false;
        Map<String, Object> hm = new HashMap<String, Object>();
        ;
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

        hm.put("FILE", MyHelper.TRACKS_ROOT + trackName + "/" + String.valueOf(steps.get(index).getID()) + ".jpg");
        hm.put("ID", String.valueOf(currentPic.getId()));

        list.add(0, hm);

        pl = new PhotoLoader(list, this);
        pl.execute(false);

        previous = null;
        next = null;

        if (currentPic.getHeight() > 0) {
            Log.d(tag, "imageview metrics known, setting previous and next");
            if (index > 1) {
                Log.d(tag, "not first image, setting previous");
                previous = PhotoLoader.sizeAndOrient(MyHelper.TRACKS_ROOT + trackName + "/" + String.valueOf(steps.get(index - 1).getID()) + ".jpg", currentPic.getHeight(), currentPic.getWidth(), 4);
            }
            if (index < steps.size() - 1) {
                Log.d(tag, "not last image, setting next");
                next = PhotoLoader.sizeAndOrient(MyHelper.TRACKS_ROOT + trackName + "/" + String.valueOf(steps.get(index + 1).getID()) + ".jpg", currentPic.getHeight(), currentPic.getWidth(), 4);
            }
        } else {
            Log.d(tag, "current height of imageview unknown");
        }

        pos = index;
        ready = true;
    }

    private void goPrevious() {
        if (pos > 0) {
            Log.d(tag, "getting previous image");
            next = ((BitmapDrawable) currentPic.getDrawable()).getBitmap();
            if (previous == null) {
                currentPic.setImageBitmap(PhotoLoader.sizeAndOrient(MyHelper.TRACKS_ROOT + trackName + "/" + String.valueOf(steps.get(pos - 1).getID()) + ".jpg", currentPic.getHeight(), currentPic.getWidth(), 3));
            } else {
                currentPic.setImageBitmap(previous);
            }
            pos--;
            if (pos > 0) {
                previous = PhotoLoader.sizeAndOrient(MyHelper.TRACKS_ROOT + trackName + "/" + String.valueOf(steps.get(pos - 1).getID()) + ".jpg", currentPic.getHeight(), currentPic.getWidth(), 4);
            }
            ready = true;
        }
    }

    private void goNext() {
        if (pos < steps.size() - 1) {
            Log.d(tag, "getting next image");
            previous = ((BitmapDrawable) currentPic.getDrawable()).getBitmap();
            if (next == null) {
                Log.i(tag, "Loading image from file");
                currentPic.setImageBitmap(PhotoLoader.sizeAndOrient(MyHelper.TRACKS_ROOT + trackName + "/" + String.valueOf(steps.get(pos + 1).getID()) + ".jpg", currentPic.getHeight(), currentPic.getWidth(), 4));
            } else {
                Log.i(tag, "Loading image from memory");
                currentPic.setImageBitmap(next);
            }
            pos++;
            if (pos < steps.size() - 1) {
                next = PhotoLoader.sizeAndOrient(MyHelper.TRACKS_ROOT + trackName + "/" + String.valueOf(steps.get(pos + 1).getID()) + ".jpg", currentPic.getHeight(), currentPic.getWidth(), 3);
            }
            ready = true;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_play:
                play = true;
                playback.setVisibility(View.GONE);
                if (!playThread.isAlive()) {
                    playThread = new Thread() {
                        @Override
                        public void run() {
                            while (play) {
                                if (ready) {
                                    try {
                                        Log.i(tag, "sleeping....");
                                        this.sleep(2000);
                                        Log.i(tag, "....awake");
                                        if (play) {
                                            ready = false;
                                            Message m = playHandler.obtainMessage(1);
                                            m.sendToTarget();
                                        }
                                    } catch (Exception e) {
                                        Log.e(tag, "error in playThread", e);
                                    }
                                }
                            }
                        }
                    };

                    playThread.start();
                }
                break;
            case R.id.btn_end:
                loadImage(steps.size() - 1);
                break;
            case R.id.btn_beginning:
                loadImage(0);
                break;
        }
    }

    @Override
    public boolean onTouch(View arg0, MotionEvent arg1) {
        Log.i(tag, "touch event detected");
        gd.onTouchEvent(arg1);
        return true;
    }

    @Override
    public void onPause() {
        play = false;
        ready = false;
        super.onPause();
    }


    private class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            Log.i(tag, "Fling event detected");
            if (velocityX > 800) {
                //Swipe right
                play = false;
                playback.setVisibility(View.VISIBLE);
                goPrevious();
            } else if (velocityX < -800) {
                //Swipe left
                play = false;
                playback.setVisibility(View.VISIBLE);
                goNext();
            }
            return true;
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            Log.i(tag, "tap event detected");
            play = false;
            playback.setVisibility(View.VISIBLE);
            return super.onSingleTapUp(e);
        }
    }
}
