package com.LupusSolus.ProgressTracker;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.widget.ImageButton;
import android.widget.Toast;

import com.LupusSolus.ProgressTracker.dialogs.ConfimDeleteDialog;
import com.LupusSolus.ProgressTracker.models.Track;
import com.LupusSolus.ProgressTracker.utils.MyHelper;
import com.LupusSolus.ProgressTracker.utils.TrackDB;

import java.io.File;
import java.lang.reflect.Field;

public class MainActivity extends FragmentActivity {
    /**
     * Called when the activity is first created.
     */
    // private ArrayAdapter<Track> aa;
    public static OCL ocl;

    //private static ListView trackListView;

    private static FragmentManager fm;
    private Fragment frag;
    public static Track selected;
    public static boolean deleteMode = false;

    //public static Typeface font;
    //public static Typeface boldFont;

    public static CancelDialogOnClickListener cancelDialogListener;
    private static final String tag = "Main";
    //static ImageView splash;
    //private static PowerManager.WakeLock wl;

    // Thread t;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //
        //FORCE MENU ITEMS TO OVERFLOW HACK
        //
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (getLastCustomNonConfigurationInstance() != Boolean.TRUE) {
            // Log.d(tag, "\n\n\n\t\t!!!! Flush Cache !!!!!\n\n\n");
            MyHelper.bitmapCache.evictAll();
        }

        setContentView(R.layout.main);

        cancelDialogListener = new CancelDialogOnClickListener();

        try {
            ImageButton firstTrackBtn = (ImageButton) findViewById(R.id.btn_firstTrack);
            TrackDB.open(this);

            ocl = new OCL(this);
            //firstTrackBtn.setOnClickListener(ocl);
        } catch (SQLException ex) {
            Toast.makeText(this, ex.toString(), Toast.LENGTH_LONG).show();
        }

        fm = getSupportFragmentManager();
        int orientation = getResources().getConfiguration().orientation;

        //if(orientation == Configuration.ORIENTATION_PORTRAIT){
        frag = new MainListFragment();
        //}else if(orientation == Configuration.ORIENTATION_LANDSCAPE){
        //    frag = new MainGridFragment();
        //}

        //frag.setRetainInstance(true);

        fm.beginTransaction().replace(R.id.main_list, frag).commit();
    }

    @Override
    protected void onResume() {
        //deleteMode = false;
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mi = getMenuInflater();
        mi.inflate(R.menu.tracklistmenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_newtrack:
                startActivity(new Intent(this, AddTrackActivity.class));
                break;
            case R.id.menu_deltrack:
                deleteMode = true;
                fm.beginTransaction().replace(R.id.main_list, new MainListFragment()).commit();
                break;
            case R.id.menu_settings:
                startActivity(new Intent(this, AlarmSettingsActivity.class));
                break;
            case R.id.menu_searchtrack:
                showNotFunctional(this);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (deleteMode) {
            deleteMode = false;

//            int orientation = getResources().getConfiguration().orientation;
//
//            if(orientation == Configuration.ORIENTATION_PORTRAIT){
//                frag = new MainListFragment();
//            }else if(orientation == Configuration.ORIENTATION_LANDSCAPE){
//                frag = new MainGridFragment();
//            }

            fm.beginTransaction().replace(R.id.main_list, frag).commit();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return Boolean.TRUE;
    }

    private void showNotFunctional(Context c) {
        Toast.makeText(c, "Not functional", Toast.LENGTH_SHORT).show();
    }

    private static class OCL implements OnClickListener {
        private Context mCtx;

        public OCL(Context ctx) {
            mCtx = ctx;
        }

        @Override
        public void onClick(View arg0) {
            switch (arg0.getId()) {
                case R.id.btn_firstTrack:
                    Intent i = new Intent(mCtx, FirstTrackActivity.class);
                    mCtx.startActivity(i);
                    break;
                case R.id.tracklist_delete:
                    try {
                        selected = TrackDB.findTrack(((Integer) arg0.getTag()).intValue());
                        new ConfimDeleteDialog().show(fm, "confirm_delete");
                    } catch (Exception e) {
                        Log.e(tag, "Error deleting track", e);
                    }
                    break;
            }
        }
    }

    private class CancelDialogOnClickListener implements Dialog.OnClickListener {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    Log.d(tag, "YOU SAID DELETE TRACK!!");
                    try {
                        TrackDB.deleteTrack(selected);
                        File trackDir = new File(MyHelper.TRACKS_ROOT + selected.getName());
                        File[] files = trackDir.listFiles();

                        if (files != null) {
                            for (int index = 0; index < files.length; index++) {
                                files[index].delete();
                            }
                        }
                        fm.beginTransaction().replace(R.id.main_list, new MainListFragment()).commit();

                        trackDir.delete();
                        break;
                    } catch (Exception e) {
                        Log.e(tag, "Error deleting track", e);
                    }
                case DialogInterface.BUTTON_NEGATIVE:
                default:

                    break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        //MyHelper.bitmapCache.evictAll();
        super.onDestroy();
    }
}
